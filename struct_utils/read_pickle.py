#!/usr/bin/env python3

import pandas
from ase import io
import sys
import os
from common import read_qe_settings, qe_format_value

selected = pandas.read_pickle("selected-al-gen5.pkl.gz")

#print(selected)
print(selected['ase_atoms'])
a = selected['ase_atoms'].values

params, pps, kpts, koffset = read_qe_settings('pw.in')

i = 0
for struct in a:
	d = i // 100
	try:
		os.mkdir(str(d))
	except:
		print("already exists")
	cell = struct.cell.cellpar()
	# exploded, skipping
	if cell[0] > 19 or cell[1] > 19 or cell[2] > 19:
		print("skipping too large")
		continue
        # imploded skipping
	density = sum(struct.get_masses()) * 1.66053904e-24 / struct.get_volume() / 1e-24
	symbols = struct.get_chemical_symbols()
	percW = float(symbols.count('W')) / len(symbols) * 100
	max_dens = (percW * percW * -0.00153 + percW * 0.307317 + 3.05753) * 1.3
	if density > max_dens:
		print("skipping too dense")
		continue 
	kpts = [int(round(40.0/x)) for x in cell[:3]]
	print(kpts[:3])
	c = i % 100
	try:
		os.mkdir(str(d) + '/' + str(c))
	except:
                print("already exists")
	print(d,c)
	io.write(str(d) + '/' + str(c) + '/pw.in', struct, format='espresso-in', input_data=params, pseudopotentials=pps, kpts=kpts, koffset=koffset)
	i += 1
