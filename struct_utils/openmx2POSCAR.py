#!/usr/bin/env python3

from sys import argv
from math import *
import argparse
from common import readopenmx
from ase import io

parser = argparse.ArgumentParser(description='Convert OpenMX .dat(#) files into Wien2k struct files')
parser.add_argument('-n', help='number all atoms', action='store_true')
parser.add_argument("input_file", help='path to the input .dat file')
args = parser.parse_args()

s = readopenmx(args.input_file)
io.write('-', s, format="vasp", direct=1)
