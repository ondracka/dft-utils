#!/usr/bin/env python3

from ase import io, neighborlist 
import sys
import numpy as np

atoms = io.read(sys.argv[1], format='cif')
print (atoms)
nat = len(atoms)
#minimum boron index
minb = 999999999
nb = 0
for b,a in enumerate(atoms):
    if a.symbol == "B":
        nb += 1
        if b < minb:
            minb = b
print("Minbindex ", minb)

cutoffs = neighborlist.natural_cutoffs(atoms, mult=1)
cutoffs = {('B', 'B'): 2.0, ('B','W'): 1.0, ('B','C'): 1.0, ('W', 'W'): 1.0, ('W', 'C'): 1.0, ('C','C'):1.0}
nlist = neighborlist.neighbor_list(('i','j'), atoms, cutoffs)

nlistx = [x - minb for x in list(nlist[0])]
setx = set(nlistx)
allnums = set(range(nb))
separate = allnums.difference(setx)

nlisty = [x - minb for x in list(nlist[1])]
print(nlistx, nlisty)

#matrix = nlist.get_connectivity_matrix()
clusters = []

for i,c in enumerate(nlistx):
    inserted = False
    #pick first atom from list
    d = nlisty[i]
    #print("trying to insert", c, d)
    for j,oldc in enumerate(clusters):
        #print (oldc)
        if d in oldc or c in oldc:
            clusters[j].add(c)
            clusters[j].add(d)
            inserted = True
            continue
    if not inserted:
        #print("new cluster")
        cluster = {c, nlisty[i]};
        clusters.append(cluster)

print (clusters)

nclusters = 0

while (len(clusters) != nclusters):
    newclusters = []
    for c in clusters:
        inserted = False
        if newclusters == []:
            #print("inserted first:", c)
            newclusters.append(c)
        else:
            for i,nc in enumerate(newclusters):
                #print(c, nc, c.intersection(nc))
                if len(c.intersection(nc)) != 0:
                    #print("have intersection")
                    inserted = True
                    newclusters[i].update(c);
            if not inserted:
                newclusters.append(c)

    nclusters = len(clusters)
    clusters = newclusters

print (clusters)
print (separate)

csum = len(separate)
n = len(separate) + len(clusters)
for c in clusters:
    csum += len(c)

print(float(csum)/float(n))
