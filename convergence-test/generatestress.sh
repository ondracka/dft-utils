#!/bin/bash

#job for stress strain method
#first argument is numeric "123" or "2"
#and means what epsilons will be used, 1 = 0.007, 2 = 0.014, 3 = 0.021
#second is a sbatch script to be called in the subfolders

## Job name
#SBATCH --job-name=vasp-job

### File for the output
#SBATCH --output=JOB_OUTPUT

### Time your job needs to execute, e. g. 30 min
#SBATCH --time=02:00:00

### Use one node for parallel jobs on shared-memory systems
#SBATCH --nodes=1

### Number of threads to use, e. g. 24
#SBATCH --cpus-per-task=1

### Number of hyperthreads per core
#SBATCH --ntasks-per-core=1

### Tasks per node (for shared-memory parallelisation, use 1)
#SBATCH --ntasks-per-node=24

#SBATCH --account=jara0151

module load CHEMISTRY
module load vasp

srun vasp_mpi &> OUT || exit

#this is recomended by https://www.vasp.at/wiki/index.php/Energy_vs_volume_Volume_relaxations_and_Pulay_stress
# section "Accurate bulk relaxations with internal parameters (one)"
if [ -f "CONTCAR"]
then
	mv CONTCAR POSCAR
fi
srun vasp_mpi &> OUT2 || exit

mkdir stress
mkdir stress/eq
cp INCAR KPOINTS OUTCAR POTCAR stress/eq/
cp CONTCAR stress/eq/

cd stress ||exit
python3 ~/work/DFT/Cij/prep_Cij.py

if ! grep "1" <<< "$1"
then
	rm -rf eps=0.007
fi
if ! grep "2" <<< "$1"
then
	rm -rf eps=0.014
fi
if ! grep "3" <<< "$1"
then
	rm -rf eps=0.021
fi

for i in eps*/*
do
	cp eq/KPOINTS $i/
        cd $i || exit
       
	sbatch $2
                         
	cd ../..
done
