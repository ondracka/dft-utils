#!/usr/bin/env python

from math import radians,cos,sin,sqrt
import numpy as np

BohrtoA = 0.529177249

def cell_volume(c):
    return c['a'] * c['b'] * c['c'] * sqrt(1 - cos(c['alpha'])**2 - cos(c['beta'])**2 - cos(c['gamma'])**2 + 2 * cos(c['alpha']) * cos(c['beta']) * cos(c['gamma']))

def frac2cart(pos, c):
    x = c['a'] * pos[0] + c['b'] * cos(c['gamma']) * pos[1] + c['c'] * cos(c['beta']) * pos[2]
    y = c['b'] * sin(c['gamma']) * pos[1] + c['c'] * (cos(c['alpha']) - cos(c['beta']) * cos(c['gamma'])) / sin(c['gamma']) * pos[2]
    z = cell_volume(c) / c['a'] / c['b'] / sin(c['gamma']) * pos[2]
    return np.array([x,y,z])

def dist(a1, a2, cell):
    "determines the distance between two atoms"
    #FIXME: stupid stuff right now, optimize!
    A = frac2cart([1,0,0],cell)
    B = frac2cart([0,1,0],cell)
    C = frac2cart([0,0,1],cell)

    dmin=999999

    for i in range(-1,2):
        for j in range(-1,2):
            for k in range(-1,2):
                dist = np.linalg.norm(a1[1] - a2[1] + i*A + j*B + k*C)
                if dist < dmin:
                    dmin = dist

    return dmin

def distfrac(a1, a2, cell):
    dmin=999999
    A = np.array([1,0,0])
    B = np.array([0,1,0])
    C = np.array([0,0,1])

    for i in range(-1,2):
        for j in range(-1,2):
            for k in range(-1,2):
                p1 = frac2cart(a1[1], cell)
                p2 = frac2cart(a2[1] + i*A + j*B + k*C, cell)
                dist = np.linalg.norm(p1 - p2)
                if dist < dmin:
                    dmin = dist

    return dmin


def read_struct_file(f):
    """Read position of atoms from input file f"""

    lines = [line for line in open(f)]
    cell = {}

    #check what file we have, UPDATE the xyz reading
    if f.endswith(".xyz"):
        natoms = int(lines[0])
        print("We have %i atoms" % natoms)

        rawdata = [ lines[x].split() for x in range (2, natoms + 2) ]
        data = [[rawdata[x][0], float(rawdata[x][1]), float(rawdata[x][2]), float(rawdata[x][3])] for x in range(0, natoms)]
        tmp = [float(line) for line in open("dims")]
        cell['a'] = tmp[0]
        cell['b'] = tmp[1]
        cell['c'] = tmp[2]

    elif f.endswith(".struct"):
        #FIXME: does this hadle different mutiplicities?
        data = []
        natoms = 0
        atype = ""
        cmult = 0
        atomblock = 1
        for i,l in enumerate(lines):
            if i == 3:
                cell['a'] = float(l[0:9]) * BohrtoA
                cell['b'] = float(l[10:19]) * BohrtoA
                cell['c'] = float(l[20:29]) * BohrtoA
                cell['alpha'] = radians(float(l[30:39]))
                cell['beta'] = radians(float(l[40:49]))
                cell['gamma'] = radians(float(l[50:59]))
            if i > 3:
                #print l
                if l[0:4] == "ATOM":
                    #print "atom"
                    atomblock = i
                    t = l.split()
                    if len(t) == 5:
                       t = [float(t[x].split("=")[1]) for x in range(2,5,1)]
                    elif len(t) == 4:
                       t = [float(t[x].split("=")[1]) for x in range(1,4,1)]
                    else:
                       print("problem!")
                    t = frac2cart(t,cell)
                    data.append(["", t])
                elif l[10:14] == "MULT":
                    #print "mult"
                    cmult = int(l[15:17])
                elif i > atomblock + cmult + 4:
                    #print "finished reading"
                    break
                elif i < atomblock + 1 + cmult:
                    #print "reading atom position"
                    t = l.split()
                    t = [float(t[x].split("=")[1]) for x in range(1,4,1)]
                    t = frac2cart(t,cell)
                    data.append(["", t])
                elif i == atomblock + 1 + cmult:
                    #print "reading atom type"
                    atomtype = l[0:2].strip()
                    for j in range(-cmult,0,1):
                        data[j][0] = atomtype

    atomtypes = list(set(a[0] for a in data))

    return [data, cell, atomtypes]
