#!/bin/bash

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright (c) 2016-2021 Pavel Ondračka.

function help {
   echo "Script to generate input for convergence tests"
   echo "The tested parameters are used together with the rest of the input in start directory"
   echo ""
   echo "Usage: gen-conv-parse [option]"
   echo ""
   echo "Supported conv parameters"
   echo "-o openmx:"
   echo "-k kpoints \"1x1x1;2x2x2; ...\""
	echo "-c convergence criterion \"1e-5 1e-6 ...\""
	echo "-e energy cutoff \" 200 300 ...\""
   echo ""
   echo "-q Quantum ESPRESSO:"
   echo "-k kpoints \"1x1x1;2x2x2; ...\""
	echo "-c convergence criterion \"1e-4 1e-5 ...\" (Ry)"
   echo "-e planewave energy cutoff \" 30 40 ...\" (Ry)"
   echo "-g kinetic energy cutoff for charge density \"100 150 ...\" (Ry)"
   echo "-H cutoff for hybrid hf \"100 150 ...\" (Ry)"
   echo "-F force convergence criterion \"1e-2 1e-3\" (Ha/Bohr)"
   echo "-b number of bands \"12 14 ...\""
   echo "-m smearing \"0.01 0.02 ...\" (Ry)"
   echo ""
   echo "-w Wien2k"
   echo "-k kpoints \"1x1x1;2x2x2; ...\""
   echo "-c convergence criterion \"1e-5 1e-6 ...\""
   echo "-r RKmax \"5.0 6.0 6.5\""
   echo "-f FFT grid density \"1.0 2.0 3.0 ...\""
   echo "-g Gmax \"10.0 12.0 14.0 ...\""
   echo "-l lvns \"2 4 6 8 ...\""
   echo ""
   echo "-V VASP:"
   echo "-k kpoints \"1x1x1;2x2x2; ...\""
	echo "-c energy convergence criterion \"1e-5 1e-6 ...\""
	echo "-F force convergence criterion \"-0.01 -0.02\""
	echo "-e energy cutoff \" 200 300 ...\""
	echo "-S spin \"1 2\" (1 = no spin polariation, 2 = collinear spin polarization)" 
   echo ""
   echo "-s submit immediatelly \"submit command\""
   echo "-h print this help and exit"
   echo "-v output more information"
}

debug_echo () {
	if [ "$DEBUG" == 1 ]
	then
		echo $1
	fi
}

check_default () {
   debug_echo "check_default: checking $1 $2"

   if [ "$CODE" == "w2k" ]
   then
      if [ "$1" == 'kpoint' ]
      then
         VAL=$(grep -o "(.*)" ${CASE}.klist | grep -o "[0-9 ]*" | awk '{printf("%ix%ix%i", $1, $2, $3)}')
         test "$VAL" == "$2"
         RET=$?
      elif [ "$1" == 'RKmax' ]
      then
         VAL=$(awk '{if(NR == 2){print $1}}' ${CASE}.in1${COMPLEX})
         test $(echo "$2 == $VAL" | bc) == "1"
         RET=$?
      elif [ "$1" == 'lvns' ]
      then
         VAL=$(awk '{if(NR == 2){print $3}}' ${CASE}.in1${COMPLEX})
         test "$VAL" == "$2"
         RET=$?
      elif [ "$1" == 'Gmax' ]
      then
         VAL=$(grep "GMAX" ${CASE}.in2${COMPLEX} | grep -o "[0-9]*\.[0-9]*")
         test $(echo "$2 == $VAL" | bc) == "1"
         RET=$?
      elif [ "$1" == 'eneconv' ]
      then
         echo "FIXME: for wien2k this need to be set at runtime"
      else
         echo "check_default: Unsupported type"
         return 1
      fi
   elif [ "$CODE" == "VASP" ]
   then
      if [ "$1" == 'kpoint' ]
      then
         echo "Warning: setting kpoints works just with the automatic grid"
         VAL=$(sed -n '4p' KPOINTS | grep -Eo '[0-9]+\s*[0-9]+\s*[0-9]+' | sed 's/  */x/g')
         echo "detected $VAL selected $2"
         test "$VAL" == "$2"
         RET=$?
      elif [ "$1" == 'cutoff' ]
      then
         VAL=$(grep 'ENCUT' INCAR | grep -o "[0-9]*")
         test "$VAL" == "$2"
         RET=$?
      elif [ "$1" == 'ispin' ]
      then
         VAL=$(grep 'ISPIN' INCAR | grep -o "[0-9]")
         test "$VAL" == "$2"
         RET=$?
      elif [ "$1" == 'forceconv' ]
      then
         VAL=$(grep 'EDIFFG' INCAR | grep -Eo "\-*[0-9]+\.*[0-9]*[eE]*-*[0-9]*")
         debug_echo "check_default: EDIFFG value $2, default $VAL"
         RET=$(awk "END{if ($2 == $VAL) {print 0} else {print 1}}" <<< "")
      elif [ "$1" == 'eneconv' ]
      then
         VAL=$(grep 'EDIFF[ =]' INCAR | grep -Eo "[0-9]+\.*[0-9]*[eE]*\-*[0-9]*")
         debug_echo "check_default: EDIFF value $2, default $VAL"
         RET=$(awk "END{if ($2 == $VAL) print 0; else print 1}" <<< "")
       fi
   elif [ "$CODE" == "openmx" ]
   then
      if [ "$1" == 'kpoint' ]
      then
         VAL=$(grep "scf.Kgrid" *.dat | grep -Eo '[0-9]+\s*[0-9]+\s*[0-9]+' | sed 's/  */x/g')
         echo "detected $VAL selected $2"
         test "$VAL" == "$2"
         RET=$?
      elif [ "$1" == 'cutoff' ]
      then
         VAL=$(grep 'scf.energycutoff' *.dat | awk '{print $2}')
         debug_echo "check_default: scf.energycutoff value $2, default $VAL"
         RET=$(awk "END{if ($2 == $VAL) print 0; else print 1}" <<< "")
      elif [ "$1" == 'eneconv' ]
      then
         VAL=$(grep 'scf.criterion' *.dat | awk '{print $2}')
         debug_echo "check_default: scf.criterion value $2, default $VAL"
         RET=$(awk "END{if ($2 == $VAL) print 0; else print 1}" <<< "")
      fi
   elif [ "$CODE" == "qe" ]
   then
      if [ "$1" == 'eneconv' ]
      then
         VAL=$(grep '^ *conv_thr *= *[-0-9\.eEd]*' *.in | grep -Eo "[0-9]+\.*[0-9]*[eEd]*\-*[0-9]*" | sed 's/[dD]/e/g')
         debug_echo "check_default: conv_thr value $2, default $VAL"
         RET=$(awk "END{if ($2 == $VAL) print 0; else print 1}" <<< "")
      elif [ "$1" == 'cutoff' ]
      then
         VAL=$(grep 'ecutwfc[ =]*[-0-9\.eEd]*' *.in | grep -Eo "[0-9]+\.*[0-9]*[eEd]*\-*[0-9]*" | sed 's/[dD]/e/g')
         debug_echo "check_default: ecutwfc value $2, default $VAL"
         RET=$(awk "END{if ($2 == $VAL) print 0; else print 1}" <<< "")
      elif [ "$1" == 'Gmax' ]
      then
         VAL=$(grep 'ecutrho[ =]*[-0-9\.eEd]*' *.in | grep -Eo "[0-9]+\.*[0-9]*[eEd]*\-*[0-9]*" | sed 's/[dD]/e/g')
         debug_echo "check_default: ecutrho value $2, default $VAL"
         RET=$(awk "END{if ($2 == $VAL) print 0; else print 1}" <<< "")
      elif [ "$1" == 'ecutfock' ]
      then
         VAL=$(grep 'ecutfock[ =]*[-0-9\.eEd]*' *.in | grep -Eo "[0-9]+\.*[0-9]*[eEd]*\-*[0-9]*" | sed 's/[dD]/e/g')
         debug_echo "check_default: ecutfock value $2, default $VAL"
         RET=$(awk "END{if ($2 == $VAL) print 0; else print 1}" <<< "")
      elif [ "$1" == 'forceconv' ]
      then
         VAL=$(grep 'forc_conv_thr[ =]*[-0-9\.eEd]*' *.in | grep -Eo "[0-9]+\.*[0-9]*[eEd]*\-*[0-9]*" | sed 's/[dD]/e/g')
         debug_echo "check_default: forc_conv_thr value $2, default $VAL"
         RET=$(awk "END{if ($2 == $VAL) print 0; else print 1}" <<< "")
      elif [ "$1" == 'bands' ]
      then
         VAL=$(grep 'nbnd[ =]*[0-9]*' *.in | grep -Eo "[0-9]+")
         debug_echo "check_default: bands value $2, default $VAL"
         RET=$(awk "END{if ($2 == $VAL) print 0; else print 1}" <<< "")
      elif [ "$1" == 'smearing' ]
      then
         VAL=$(grep 'degauss[ =]*[-0-9\.]*' *.in | grep -Eo "[-0-9\.]+")
         debug_echo "check_default: smearing value $2, default $VAL"
         RET=$(awk "END{if ($2 == $VAL) print 0; else print 1}" <<< "")
      elif [ "$1" == 'kpoint' ]
      then
         if grep -q "K_POINTS *automatic" *.in
         then
            VAL=$(grep -A1 "K_POINTS *automatic" *.in | tail -n1 | awk '{printf "%ix%ix%i", $1, $2, $3}')
         else
            echo "check_default: Only automatic k-point grid supported"
            exit 6
         fi
         debug_echo "check_default: kpoints value $2, default $VAL"
         test "$VAL" == "$2"
         RET=$?
      fi
   else
      echo "check_default: Unsupported code"
      return 1
   fi

   if [ "$RET" ==  0 ]
   then
      debug_echo "check_default: $1 value of $2 is the default"
   else
      debug_echo "check_default: $1 value of $2 is not the default value"
   fi
   return $RET
}

modify_input () {
   debug_echo "modify_input: $1 to $2"

   #OpenMX stuff
   if  [ "$CODE" == "openmx" ]
   then
      if [ "$1" == 'kpoint' ]
      then
         ARG=$(printf "$2" | sed 's/x/ /g')
         sed -i "s/\(scf\.Kgrid \).*/\1${ARG}/g" *.dat
      elif [ "$1" == 'cutoff' ]
      then 
         sed -i "s/\(scf\.energycutoff \).*/\1${2}/g" *.dat
      elif [ "$1" == 'eneconv' ]
      then 
         sed -i "s/\(scf\.criterion \).*/\1${2}/g" *.dat
      fi

   #Wien2k stuff
   elif [ "$CODE" == "w2k" ]
   then
      if [ "$1" == 'kpoint' ]
      then
         TMP=$(sed 's/x/\n/g' <<< "$2")
         TMP=$(printf "0\n${TMP}\n1")
         SCRATCH=. x kgen <<< "$TMP"
      elif [ "$1" == 'RKmax' ]
      then
         NUM=$(printf "%6.2f" "$2")
         sed -i "2s/^....../$NUM/g" ${DNAME}.in1$COMPLEX
      elif [ "$1" == 'lvns' ]
      then
         sed -i "2s/^\(\s*[0-9]*\.[0-9]*\s*[0-9]*\s*\)[0-9]*\(.*\)/\1${2}\2/g" ${DNAME}.in1$COMPLEX
      elif [ "$1" == 'Gmax' ]
      then
         NUM=$(printf "%6.2f" "$2")
         sed -i "s/^ *[0-9]*\.[0-9]*\( *GMAX\)/$NUM\1/g" ${DNAME}.in2$COMPLEX
      elif [ "$1" == 'eneconv' ]
      then
         echo "FIXME: for Wien2k this need to be set at runtime"
      fi

   #VASP stuff
   elif [ "$CODE" == "VASP" ]
   then
      if [ "$1" == 'kpoint' ]
      then
         echo "Warning: setting kpoints works just with the automatic grid"
         XTMP=$(sed 's/x/ /g' <<< "$2")
         sed -i "4s/.*/${XTMP}/g" KPOINTS
      elif [ "$1" == 'cutoff' ]
      then
         sed -i "s/ENCUT.*/ENCUT = $2/g" INCAR
      elif [ "$1" == 'eneconv' ]
      then
         sed -i -E "s/EDIFF[ =]+.*/EDIFF = $2/g" INCAR
      elif [ "$1" == 'forceconv' ]
      then
         sed -i "s/EDIFFG[ =]*.*/EDIFFG = $2/g" INCAR
      elif [ "$1" == 'ispin' ]
      then
         sed -i "s/ISPIN[ =]*.*/ISPIN = $2/g" INCAR
      fi
   #qe stuff
   elif [ "$CODE" == "qe" ]
   then
      if [ "$1" == 'eneconv' ]
      then
         sed -i "s/^ *conv_thr[ =]*[-0-9\.eEd]*/conv_thr = $2/g" *.in
      elif [ "$1" == 'forceconv' ]
      then
         sed -i "s/forc_conv_thr[ =]*[-0-9\.eEd]*/forc_conv_thr = $2/g" *.in
      elif [ "$1" == 'cutoff' ]
      then
         sed -i "s/ecutwfc[ =]*[-0-9\.eEd]*/ecutwfc = $2/g" *.in
      elif [ "$1" == 'bands' ]
      then
         sed -i "s/nbnd[ =]*[0-9]*/nbnd = $2/g" *.in
      elif [ "$1" == 'smearing' ]
      then
         sed -i "s/degauss[ =]*[0-9\.]*/degauss = $2/g" *.in
      elif [ "$1" == 'Gmax' ]
      then
         sed -i "s/ecutrho[ =]*[-0-9\.eEd]*/ecutrho = $2/g" *.in
      elif [ "$1" == 'ecutfock' ]
      then
         sed -i "s/ecutfock[ =]*[-0-9\.eEd]*/ecutfock = $2/g" *.in
      elif [ "$1" == 'kpoint' ]
      then
         TMP=$(grep -n "K_POINTS *automatic" *.in | grep -o "[0-9]*")
         TMP2=$(sed 's/x/ /g' <<< "$2")
         sed -i "$((TMP + 1))s/.*/${TMP2} 0 0 0/g" *.in
      else
         echo "modify_input: unsupported switch $1 for $CODE"
         return 4
      fi
   else
      echo "modify_input: Unsupported code!"
      return 5
   fi
}


DEBUG=0
CHECK=""
TYPES=""
CODE=""

while getopts "h?vk:c:m:e:s:owVqr:f:g:l:S:F:b:H:" opt; do
   case "$opt" in
   h|\?)
      help
      exit 0
      ;;
   v) DEBUG=1
      ;;
   w) CODE="w2k"
      ;;
   o) CODE="openmx"
      ;;
   V) CODE="VASP"
      ;;
   q) CODE="qe"
      ;;
   k) kpoint="$OPTARG"
      TYPES="$TYPES kpoint"
      ;;
   c) eneconv="$OPTARG"
      TYPES="$TYPES eneconv"
      ;;
   r) RKmax="$OPTARG"
      TYPES="$TYPES RKmax"
      ;;
   f) fftgrid="$OPTARG"
      TYPES="$TYPES fftgrid"
      ;;
   g) Gmax="$OPTARG"
      TYPES="$TYPES Gmax"
      ;;
   H) ecutfock="$OPTARG"
      TYPES="$TYPES ecutfock"
      ;;
   l) lvns="$OPTARG"
      TYPES="$TYPES lvns"
      ;;
   e) cutoff="$OPTARG"
      TYPES="$TYPES cutoff"
      ;;
   m) smearing="$OPTARG"
      TYPES="$TYPES smearing"
      ;;
   F) forceconv="$OPTARG"
      TYPES="$TYPES forceconv"
      ;;
   S) ispin="$OPTARG"
      TYPES="$TYPES ispin"
      ;;
   b) bands="$OPTARG"
      TYPES="$TYPES bands"
      ;;
   s) SUBMITCOM="$OPTARG"
      ;;
   esac
done
shift $((OPTIND-1))

#if (($# < 1))
#then
#   echo "Error: No arguments given"
#   help
#   exit 0
#fi

# Wien2k specific FIXME: maybe in some if
CASE=$(basename $PWD)
# check if we have a complex case
if [ -f ${CASE}.in1c ]
then
   debug_echo "Complex case detected"
   COMPLEX="c"
fi

#iterate over all convergence parameters
for T in $TYPES
do
   if [ "$T" == "kpoint" ]
   then
      export IFS=';'
   else
      export IFS=' '
   fi

   read -ra L <<< "${!T}"
   export IFS=' '

   #iterate over all convergence values V
   for V in "${L[@]}"
   do
      DNAME="${T}_$V"

      #do not rerun the default parameters to save time
      if check_default "$T" "$V"
      then
         #create symlink to the default directory to ease the parsing
         ln -s default/ "$DNAME"
         if [ "$HAVE_DEFAULT" == "1" ]
         then
            continue
         else
            DNAME="default"
            HAVE_DEFAULT="1"
         fi
      fi
      
      debug_echo "creating directory $DNAME"
      if [ "$CODE" == "openmx" ]
      then
         mkdir "$DNAME"
         cp *.dat "$DNAME/"
      elif [ "$CODE" == "w2k" ]
      then
         save_lapw -d $DNAME $DNAME &> /dev/null
         if [ -f ".lcore" ]
         then
            touch "$DNAME/.lcore"
         fi
      elif [ "$CODE" == "VASP" ]
      then
         mkdir "$DNAME"
         cp KPOINTS POSCAR INCAR POTCAR "$DNAME/"
      elif [ "$CODE" == "qe" ]
      then
         mkdir "$DNAME"
         INFILE=$(ls *.in)
         cp *.in "$DNAME/"
         PSEUDO_DIR=$(grep "pseudo_dir"  pw.in | grep -o "[\'\"][_a-Z0-9/\.-]*[\'\"]$" | tr -d \'\")
         if [ "$PSEUDO_DIR" == "" ]
         then
             echo "pseudo_dir must be specified"
             exit 13
         fi
         if [ "${PSEUDO_DIR:0:1}" != "/" ]
         then
             PSEUDO_DIR=$PWD/$PSEUDO_DIR
             awk -v a="$PSEUDO_DIR" '{if ($1 == "pseudo_dir") printf "pseudo_dir = \"%s\"\n", a; else print}' $INFILE > "$DNAME/$INFILE"
         fi
      fi

      cd "$DNAME" || echo "failed to switch directory"
      modify_input "$T" "$V"

      if [ "$SUBMITCOM" != "" ]
      then
         debug_echo "Submitting job for ${T}_$V from $PWD"
         debug_echo "$SUBMITCOM"
         $SUBMITCOM
      fi

      cd ..
   done
done
    
# vim: set tabstop=3:softtabstop=3:shiftwidth=2:
# vim: set expandtab:
