#!/usr/bin/env python3

import pandas as pd
from ase import Atoms,io

import os

per_atom_energy = {'C' : -17.41710717, 'B' : -5.84252611, 'Nb' : -329.2912 , 'Mo' : -136.30556 }

energy = []
forces = []
ase_atoms = []
energy_corrected = []

for root, dirs, files in os.walk("."):
    if "2x1x2" in root:
        print("skipping")
    for file in files:
        if file != "pw.out":
            continue
         
        print("newstruct", root + "/pw.out")
        structs = io.read(root + "/pw.out", index=slice(None), format="espresso-out")

        lastene = 0
        for i,s in enumerate(structs):
            if i != 0 and i < len(structs):
                if abs(lastene - s.get_total_energy())/len(s) < 0.0005:
                    print ("too low difference")
                    continue
                else:
                    lastene = s.get_total_energy()
                    print("OK")
            print(len(s))
            ase_atoms.append(s)
            energy.append(s.get_total_energy())
            ecor = s.get_total_energy()
            for a in s.get_chemical_symbols():
                ecor = ecor - per_atom_energy[a] * 13.605684958731
            print(s.get_total_energy(),ecor)
            energy_corrected.append(ecor)
            forces.append(s.get_forces())

# set reference energy to 0
reference_energy = 0
# collect all the data into a dictionary 
data = {'energy': energy,
        'forces': forces, 
        'ase_atoms': ase_atoms, 
        'energy_corrected': energy_corrected}
# create a DataFrame
df = pd.DataFrame(data)
# and save it 
df.to_pickle('my_data.pckl.gzip', compression='gzip', protocol=4)
