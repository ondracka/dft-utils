#!/bin/bash

for A in 0.985 0.99 0.995 1.0 1.005 1.01 1.015
do
   mkdir $A
   cp -v INCAR KPOINTS POTCAR $A/
   cd "$A"
   # need to make sure that the initial file has scale 1.0
   rescaleVASP ../CONTCAR > POSCAR || rescaleVASP ../POSCAR > POSCAR || exit
   sed -i "s/ISIF.*/ISIF = 4/g" INCAR
   sed -i "2s/.*/  $A/g" POSCAR
   sbatch $1
   cd ".."
done
