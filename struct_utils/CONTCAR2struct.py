#!/usr/bin/env python3

from sys import argv
import argparse
from common import printWIEN2k
from ase import io

parser = argparse.ArgumentParser(description='Convert VASP CONTCAR files into Wien2k struct files')
parser.add_argument('-n', help='number all atoms', action='store_true')
parser.add_argument("input_file", help='path to the CONTCAR file')
args = parser.parse_args()

s = io.read(args.input_file, format="vasp")
printWIEN2k(s,args.n)
