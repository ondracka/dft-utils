#!/usr/bin/env python3

from math import *
from numpy import std
import argparse
from common import read_structure

nele = [0, 2, 2, 2, 4, 2, 2, 4, 4]
RydtoeV = 13.605698066
HatoeV = 27.2116

def fmaxg(a1, a2, c):
	"doscstring"
	if a1 + a2 in c:
		return c[a1 + a2]
	elif a1 in c:
		return c[a1]
	else:
		return c['other']

def parse_cutoff_file(f):
	c = {}
	for line in open(f):
		el = line.split()
		if len(el) == 3:
			c[el[0] + el[1]] = float(el[2])
			c[el[1] + el[0]] = float(el[2])
		elif len(el) == 2:
			cutoffs[el[0]] = float(el[1])
		elif len(el) == 1:
			c['other'] = float(el[0])
	return c

def mean(data):
	"""Return the sample arithmetic mean of data."""
	n = len(data)
	if n < 1:
		return 0.0
	return float(sum(data))/float(n) # in Python 2 use sum(data)/float(n)

def wmean(data):
	"""Return the weighted sample arithmetic mean of data."""
	#print data
	if len(data) < 1:
		return "-"
	wsum = 0.0
	n = 0.0
	for x in data:
		wsum += x[0] * x[2]
		n += x[2]

	return wsum / n

def wstdev(data):
	"""Return the weighted stdev of data."""
	#print data
	if len(data) < 1:
		return "-"
	x0 = wmean(data)
	wsum = 0.0
	n = 0.0
	for x in data:
		wsum += (x[0] - x0)**2 * x[2]
		n += x[2]

	return (wsum * len(data) / n / (len(data) - 1) ) ** 0.5
#	return (wsum / n / len(data) ) ** 0.5

def wuncavg(data):
	 """Return the uncertainty of the weighted average."""
	 #print data
	 if len(data) < 1:
		  return "-"
	 x0 = wmean(data)
	 wsum = 0.0
	 n = 0.0
	 for x in data:
		  wsum += (x[0] - x0)**2 * x[2]
		  n += x[2]

	 return (wsum / n / len(data) ) ** 0.5


def _ss(data):
	"""Return sum of square deviations of sequence data."""
	c = mean(data)
	ss = sum((x-c)**2 for x in data)
	return ss

def pstdev(data):
	"""Calculates the population standard deviation."""
	n = len(data)
	if n < 2:
		return 0.0
	ss = _ss(data)
	pvar = ss/(n-1) # the population variance	
	return pvar**0.5

def puncert(data):
	 n = len(data)
	 if n < 2:
		  return 0.0
	 return pstdev(data)/sqrt(len(data))

def print_stats(cn,nn,suf):
	"""Prints statistics about atomic environments and average peak positions"""
	neighbor_types = cn[1]
	aname = cn[0]
	if suf != "":
		suf = "-" + suf
	f = open(args.p + aname + "-stats" + suf + ".txt", "w")

	# Give some stats about the overall levels without any division
	f.write("------- Overall peaks -------\n\n")
	lvls = {}
	for atom,lvl,E in sum(bind_enes, []):
		if neighbors[atom-1][0] == aname:
			if lvl in lvls:
				lvls[lvl].append(E)
			else:
				lvls[lvl] = []
				lvls[lvl].append(E)

	for lvl,data in lvls.items():
		f.write("level:" + str(lvl) + "\n")
		f.write("average position: " + str(mean(data)) + "\n")
		f.write("position stdev: " + str(pstdev(data)) + "\n")
		f.write("position uncertainty " + str(puncert(data)) + "\n")
		f.write("list of positions: " + "\n")
		f.write("\n".join(str(x) for x in data) + "\n\n")

	f.write("------- All types -------\n\n")

	unique_lvls=list(lvls.keys())

	for ntype in neighbor_types:
		for lvl in unique_lvls:
			lvl_enes = []
			lvl_indices = []
			for l in bind_enes[nat]:
				if ntype == neighbors[l[0] - 1][1]:
					if l[1] == lvl:
						lvl_enes.append(float(l[2]))
						lvl_indices.append(float(l[0]))
			f.write(ntype + str(lvl) + "\n")
			f.write("number of atoms in this configuration: " + str(len(lvl_enes)) + "\n")
			f.write("average position: " + str(mean(lvl_enes)) + "\n")
			f.write("position stdev: " + str(pstdev(lvl_enes)) + "\n")
			f.write("position uncertainty " + str(puncert(lvl_enes)) + "\n")
			f.write("list of positions: " + "\n")
			f.write("\n".join(str(int(lvl_indices[i]))+" "+str(x) for i,x in enumerate(lvl_enes)) + "\n\n")

	f.write("------- Simplified types based on number of neighbors -------\n\n")

	for n in nn[1]:
		for lvl in unique_lvls:
			lvl_enes = []
			lvl_indices = []
			for l in bind_enes[nat]:
				if neighbors[l[0] - 1][3] == n:
					if l[1] == lvl:
						lvl_enes.append(float(l[2]))
						lvl_indices.append(float(l[0]))
			f.write(str(n) + "-coordinated" + str(lvl) + "\n")
			f.write("number of atoms in this configuration: " + str(len(lvl_enes)) + "\n")
			f.write("average position: " + str(mean(lvl_enes)) + "\n")
			f.write("position stdev: " + str(pstdev(lvl_enes)) + "\n")
			f.write("position uncertainty " + str(puncert(lvl_enes)) + "\n")
			f.write("list of positions: " + "\n")
			f.write("\n".join(str(int(lvl_indices[i]))+" "+str(x) for i,x in enumerate(lvl_enes)) + "\n\n")

parser = argparse.ArgumentParser(description='Solve XPS data into subpeaks')
group = parser.add_mutually_exclusive_group(required=False)
group.add_argument('-cf', help='path to file with bond cutoff distances', dest='cutoff_file')
group.add_argument('-c', help='bond cutoff length (default 2.4 Angstrom)', type=float, default=2.4)
parser.add_argument('-b', help='broadening parameter in eV (default 0.4 eV)', type=float, default=0.4)
parser.add_argument('-p', help='prefix for output files', type=str, default="")
parser.add_argument('-suf', help='suffix for output files', type=str, default="")
parser.add_argument('-dE', help='energy step in eV (default 0.01 eV)', type=float, default=0.01)
parser.add_argument('-nospec', help='do not output spectra', action='store_const', const=0, default=1)
parser.add_argument('-H', help='input energies are in Hartree (instead of Rydbergs)', action='store_const', const=1, default=0)
parser.add_argument('-s', help='output statistics about atomic environment into additional files', action='store_const', const=1)
parser.add_argument('-coord', help='output atomic coordination numbers', action='store_const', const=1)
parser.add_argument("struct_file")
parser.add_argument("XPS_file")
args = parser.parse_args()

s = read_structure(args.struct_file)

neighbors = []
for i,atom in enumerate(s):
	neighbors.append([])
	neighbors[i].append(atom.symbol)
	neighbors[i].append([])
	neighbors[i].append([])

cutoffs = {}
if args.cutoff_file:
	cutoffs = parse_cutoff_file(args.cutoff_file)
else:
	cutoffs['other'] = args.c

# calculate nearest neighbors manually
for i,atom1 in enumerate(s):
	for j,atom2 in enumerate(s):
		if i == j:
			continue
		if s.get_distance(i, j, mic=True) < fmaxg(atom1.symbol, atom2.symbol, cutoffs):
			neighbors[i][1].append(atom2.symbol)
			neighbors[i][2].append(j)
	sorted(neighbors[i][1])
	sorted(neighbors[i][2])
	neighbors[i].append(len(neighbors[i][1]))
	neighbors[i][1] = "".join(neighbors[i][1])

# output coordination numbers
if args.coord:
	for a in atomtypes:
		c = []
		d = {}
		for i,n in enumerate(neighbors):
			if n[0] == a:
				c.append(n[3])
				if n[3] in d:
					d[n[3]].append(i - 63) ## FIXME: this as arbitrary
				else:
					d[n[3]] = [i - 63]
		c.sort()
		print(a, mean(c), std(c))#, c[int(round(len(c) * 0.25))] - mean(c), c[int(round(len(c) * 0.75))] - mean(c)
		print(d)

# find all unique neighboir configuration for each atom	
cnumtypes = []
numneigh = []
atomtypes = sorted(set(s.get_chemical_symbols()))
for i,atomtype in enumerate(atomtypes):
	cnumtypes.append([atomtype])
	numneigh.append([atomtype])
	cnumtypes[i].append([])
	numneigh[i].append([])
	for atom in neighbors:
		if atom[0] == atomtype:
			cnumtypes[i][1].append(atom[1])	
			numneigh[i][1].append(atom[3])
	cnumtypes[i][1] = sorted(set(cnumtypes[i][1]))
	numneigh[i][1] = sorted(set(numneigh[i][1]))

# open XPS data
bind_enes = [[] for a in atomtypes]
for line in open(args.XPS_file):
	line = line.split()
	i = atomtypes.index(line[1])
	if args.H:
		energy = HatoeV * float(line[6])
	else:
		energy = RydtoeV * float(line[6])
	level = int(line[2])
	bind_enes[i].append([int(line[0]), level, energy])
	
sigma = args.b
step = args.dE

out = []
out.append(["#energy"])
for a in cnumtypes:
	out[0].append(a)
out[0].append("total")

# iterate over list of unique atoms
for nat,type in enumerate(cnumtypes):
	if args.nospec:
		if len(bind_enes[nat]) == 0:
			print("No binding energies found for " + atomtypes[nat])
			continue
		c = 0.5 / sigma / sqrt(2 * pi) / len(s)
		minx = min([i[2] for i in bind_enes[nat]]) - 3 * sigma
		maxx = max([i[2] for i in bind_enes[nat]]) + 3 * sigma
		#open file for writing
		f = open(args.p + type[0] + "-XPS-spectra.txt", "w")
		f2 = open(args.p + type[0] + "-XPS-spectra-imp.txt", "w")
	
		#make header
		tmp = ["energy"]
		for a in type[1]:
			tmp.append(a)
		tmp.append("total")
		f.write(" ".join(tmp) + "\n")

		#make header
		tmp = ["energy   "]
		for a in numneigh[nat][1]:
			tmp.append(str(a))
		tmp.append("total")
		f2.write(" ".join(tmp) + "\n")
		 
		# calculate one energy step at a time
		for x in [minx + p * step for p in range(int(ceil((maxx - minx) / step)))]:
			#print x,
			out = [x]
			out2 = [x]

			xsum = 0.0
			# go over all atomic environments
			for j,neighbor_type in enumerate(type[1]):
				out.append(0.0)
				for l in bind_enes[nat]:
					if neighbor_type == neighbors[l[0] - 1][1]:
						tmp = c * e ** ( -0.5 * ((x - l[2]) / sigma) * ((x - l[2]) / sigma)) * nele[l[1]]
						out[j + 1] += tmp
				xsum += out[j + 1]

			# go over all possible number of neighbors
			for j,n in enumerate(numneigh[nat][1]):
				out2.append(0.0)
				for l in bind_enes[nat]:
					if neighbors[l[0] - 1][3] == n:
						tmp = c * e ** ( -0.5 * ((x - l[2]) / sigma) * ((x - l[2]) / sigma)) * nele[l[1]]
						out2[j + 1] += tmp

			out.append(xsum)
			out2.append(xsum)
			f.write(" ".join(format(x, ".5f") for x in out) + "\n")
			f2.write(" ".join(format(x, ".5f") for x in out2) + "\n")

	if args.s:
		print_stats(cnumtypes[nat], numneigh[nat], args.suf)

# vim: tabstop=3 shiftwidth=3 noexpandtab
