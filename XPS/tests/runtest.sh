#!/bin/bash

STATUS=0
GLOBALSTATUS=0

check_status () {
	if [ "$STATUS" == "0" ]
	then
		echo " OK"
	else
		echo " failed"
		GLOBALSTATUS=1
	fi
	STATUS=0
}

echo testing XPS-parse spectra generation
../XPS-parse.py TiON.struct TiON-XPS-shifts-dscf-cond.txt || STATUS=1
diff -Z N-XPS-spectra.txt XPS-parse/N-XPS-spectra.txt || STATUS=1
diff -Z O-XPS-spectra.txt XPS-parse/O-XPS-spectra.txt || STATUS=1
diff -Z Ti-XPS-spectra.txt XPS-parse/Ti-XPS-spectra.txt || STATUS=1
diff -Z N-XPS-spectra-imp.txt XPS-parse/N-XPS-spectra-imp.txt || STATUS=1
diff -Z O-XPS-spectra-imp.txt XPS-parse/O-XPS-spectra-imp.txt || STATUS=1
diff -Z Ti-XPS-spectra-imp.txt XPS-parse/Ti-XPS-spectra-imp.txt || STATUS=1
rm -rf *spectra*
printf "XPS-parse spectra"
check_status

echo testing XPS-parse statistics generation
../XPS-parse.py -s -nospec TiON.struct TiON-XPS-shifts-dscf-cond.txt || STATUS=1
diff -Z N-stats.txt XPS-parse/N-stats.txt || STATUS=1
diff -Z O-stats.txt XPS-parse/O-stats.txt || STATUS=1
diff -Z Ti-stats.txt XPS-parse/Ti-stats.txt || STATUS=1
rm -rf *stats.txt*
printf "XPS-parse stats"
check_status

if [ "$GLOBALSTATUS" == "0" ]
then
	echo All tests finished OK
	exit 0
else
	echo "Errors detected!"
	exit 1
fi
