#!/usr/bin/env python3

from math import exp, sqrt, pi, ceil

f = open('pw.out', 'r')
readweight = -10000000
weights = []
energies = []
read = False
numk = -1
skip = 0
efermi = 0.0

for line in f:
   if skip > 0:
      skip -= 1
      continue
   elif "occupation numbers" in line:
      skip = int(ceil(float(len(energies[-1])) / 8.0)) + 1
      continue
   elif "number of k points=" in line:
      numk = int(line.split()[4])
      readweight = -1
      continue
   elif readweight >= 0 and readweight < numk:
      weights.append(float(line.split()[9]))
   elif "End of self-consistent calculation" in line:
      #print("starting new energies")
      energies = []
      skip = 1
      read = True
   elif read and len(line.split()) > 0 and line.split()[0] == "k":
      #print("new kpoint")
      energies.append([])
      skip = 1
   elif "the Fermi energy is" in line:
      read = False
      efermi = float(line.split()[4])
   elif read:
      l = line.split()
      for x in l:
         energies[-1].append(float(x))
   
   readweight += 1

HOMO = -9999
LUMO = 9999
emin = 9999
emax = -9999
for i,e in enumerate(energies):
   for j,ee in enumerate(e):
      tmp = ee - efermi
      if tmp > 0 and tmp < LUMO:
         LUMO = tmp
      if tmp < 0 and tmp > HOMO:
         HOMO = tmp

print(LUMO-HOMO)

for i,e in enumerate(energies):
   for j,ee in enumerate(e):
      tmp = ee - HOMO - efermi
      energies[i][j] = tmp
      if tmp > emax:
         emax = tmp
      if tmp < emin:
         emin = tmp

sigma = 0.05
f = open('DOS.txt', 'w')
e = round(emin - 3 * sigma, 1)
c = 1.0 / sigma / sqrt(2 * pi)
while e < emax + 3 * sigma:
   I = 0.0
   for i,k in enumerate(energies):
      for eigen in k:
         I += weights[i] * c * exp(-0.5 * (e - eigen) * (e - eigen) / sigma / sigma)
   f.write(str(e) + ' ' + str(I) + '\n')
   e += 0.01
