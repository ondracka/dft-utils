#!/bin/bash

STATUS=0
GLOBALSTATUS=0

check_status () {
	if [ "$STATUS" == "0" ]
	then
		echo " OK"
	else
		echo " failed"
		GLOBALSTATUS=1
	fi
	STATUS=0
}

test_density () {
	echo testing density $1
	diff -Z <(echo $2) <(../density.py $1.dat) || STATUS=1
	cp $1.dat $1.dat###
	diff -Z <(echo $2) <(../density.py $1.dat\#\#\#) || STATUS=1
	cp $1.vasp CONTCAR
	cp $1.vasp POSCAR
	cp $1.vasp $1.poscar
	diff -Z <(echo $2) <(../density.py $1.vasp) || STATUS=1
	diff -Z <(echo $2) <(../density.py CONTCAR) || STATUS=1
	diff -Z <(echo $2) <(../density.py POSCAR) || STATUS=1
	diff -Z <(echo $2) <(../density.py $1.poscar) || STATUS=1
	rm CONTCAR POSCAR $1.poscar $1.dat###
	#Wien2k has some precision issues
	diff -Z <(echo $2 | cut -c1-7) <(../density.py $1.struct | cut -c1-7) || STATUS=1
	printf "density $1"
	check_status
}

echo testing POSCAR2openmx
../POSCAR2openmx.py TiC.vasp | diff -w TiC.dat - || STATUS=1
../POSCAR2openmx.py AlN.vasp | diff -w AlN.dat - || STATUS=1
printf CONTCAR2openmx
check_status

echo testing CONTCAR2struct
diff -Z <(tail -n +2 TiC.struct) <(../CONTCAR2struct.py TiC.vasp | tail -n +2) || STATUS=1
diff -Z <(tail -n +2 AlN.struct) <(../CONTCAR2struct.py AlN.vasp | tail -n +2) || STATUS=1
printf CONTCAR2struct
check_status

echo testing openmx2struct
diff -Z <(tail -n +2 TiC.struct) <(../openmx2struct.py TiC.dat | tail -n +2) || STATUS=1
diff -Z <(tail -n +2 AlN.struct) <(../openmx2struct.py AlN.dat | tail -n +2) || STATUS=1
diff -Z <(tail -n +2 2PC.struct) <(../openmx2struct.py 2PC.dat | tail -n +2) || STATUS=1
printf openmx2struct
check_status

echo testing openmx2POSCAR
diff -Z <(tail -n +2 TiC.vasp) <(../openmx2POSCAR.py TiC.dat | tail -n +2) || STATUS=1
#FIXME: also test the AlN but atm there are some precision issues
printf openmx2POSCAR
check_status

test_density "AlN" "3.1513639"
test_density "TiC" "4.9379584"

echo "testing amorphgen"
diff -Z <(../amorphgen.py -s 1 -d 3 -o vasp C:10) a-C.vasp || STATUS=1
diff -Z <(../amorphgen.py -s 1 -d 15 "W:5 B:3 C:2") a-WBC.dat || STATUS=1
printf amorphgen
check_status

echo "testing mergeQE"
diff -Z <(../mergeQE.py WBC.in WBC2.in) mergeQE-results1.txt || STATUS=1
diff -Z <(../mergeQE.py WBC.in WBC2.out) mergeQE-results2.txt || STATUS=1
printf mergeQE
check_status

if [ "$GLOBALSTATUS" == "0" ]
then
	echo All tests finished OK
	exit 0
else
	echo "Errors detected!"
	exit 1
fi
