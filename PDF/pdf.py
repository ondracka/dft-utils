#!/usr/bin/env python2

from sys import argv
from math import *
import numpy as np
import argparse
import re
import sys
import itertools
from read_struct import read_struct_file,frac2cart,cell_volume,dist

parser = argparse.ArgumentParser(description='Calculate pair distibution function of a struct file')
parser.add_argument('-b', help='broadening parameter in Angstroms (default 0.02 eV)', type=float, default=0.02)
parser.add_argument('-p', help='prefix for output files', type=str, default="")
parser.add_argument('-c', action='store_true', help='crystal')
parser.add_argument('-suf', help='suffix for output files', type=str, default="")
parser.add_argument('-step', help='step in A (default 0.01 A)', type=float, default=0.01)
parser.add_argument('-max', help='maximum value of for pdf (default 5 A)',type=float, default=5)
parser.add_argument('-coord', help='maximum value of for cutoff (default: not calculated)',type=float, default=0.0)
parser.add_argument("struct_file")
args = parser.parse_args()

struct_info = read_struct_file(args.struct_file)
data = struct_info[0]
cell = struct_info[1]
atomtypes = struct_info[2]
combs = [a for a in itertools.combinations_with_replacement(atomtypes, 2)]
print '# ', combs

A = frac2cart([1,0,0],cell)
B = frac2cart([0,1,0],cell)
C = frac2cart([0,0,1],cell)
sizeA = np.linalg.norm(A)
sizeB = np.linalg.norm(B)
sizeC = np.linalg.norm(C)
stepA = int(max(ceil(args.max/sizeA), 1))
stepB = int(max(ceil(args.max/sizeB), 1))
stepC = int(max(ceil(args.max/sizeC), 1))

ds=[]
ds_types=[]
neighbors=[]
for atom in data:
    neighbors.append([atom[0],[],[]])

for i,at1 in enumerate(data):
    for j,at2 in enumerate(data):
        combtype = ""
        for k,t in enumerate(combs):
            if ( at1[0] == t[0] and at2[0] == t[1] ) or ( at2[0] == t[0] and at1[0] == t[1] ):
                combtype = k
        for a in range(-stepA,stepA + 1):
            for b in range(-stepB,stepB + 1):
                for c in range(-stepC,stepC + 1):
                    ds.append(np.linalg.norm(at1[1] - at2[1] - a*A - b*B - c*C))
                    ds_types.append(combtype)

if args.coord > 0.0:
    for i,atom1 in enumerate(data):
        for j,atom2 in enumerate(data):
            if i == j:
                continue
            if dist(atom1, atom2, cell) < args.coord:
                neighbors[i][1].append(atom2[0])
                neighbors[i][2].append(j)
        sorted(neighbors[i][1])
        sorted(neighbors[i][2])
        neighbors[i].append(len(neighbors[i][1]))
        neighbors[i][1] = "".join(neighbors[i][1])
    print neighbors

    cnumtypes = []
    for i,atomtype in enumerate(atomtypes):
        cnumtypes.append([atomtype])
        cnumtypes[i].append([])
        for atom in neighbors:
            if atom[0] == atomtype:
                cnumtypes[i][1].append(atom[1])
        cnumtypes[i][1] = sorted(set(cnumtypes[i][1]))
        cnumtypes[i].append([])
        cnumtypes[i].append([])
    print cnumtypes

# sort two lists according to the first one
ds, ds_types = zip(*sorted(zip(ds, ds_types)))
ds, ds_types = (list(t) for t in zip(*sorted(zip(ds, ds_types))))

PDF = []
V = cell_volume(cell)
partdens = V/len(data)
#FIXME: why we need te renormalize by number of atoms/100
norm = 1/args.b/sqrt(2*np.pi) * args.step * partdens / len(data) * 100
normc = partdens / len(data)

if args.c:
    d = [round(a, 5) for a in ds]
#   d = sorted(d)
    xs = [0]
    ns = [0]
    alin = 0
    s_ns=[]
    for a in range(len(set(ds_types))):
        s_ns.append(0)
    for i,val in enumerate(d):
        if val < 1e-6:
            continue
        if val > args.max:
            break
        if xs[-1] == val:
            tmp = 1.0 / (4 * np.pi * val * val)
            ns[-1] += tmp
            s_ns[ds_types[i]] += tmp
        else:
            print xs[-1], ns[-1] * normc,' '.join([str(y*normc) for y in s_ns])
            xs.append(val)
            ns.append(1.0 / (4 * np.pi * val * val))
            for a in range(len(set(ds_types))):
                s_ns[a] = 0
            s_ns[ds_types[i]] = 1.0 / (4 * np.pi * val * val)

else:
    for i in range(int(args.max/args.step)+1):
        x = i*args.step
        PDF.append(0.0)
        s_PDF=[]
        for a in range(len(set(ds_types))):
            s_PDF.append(0.0)
        for j,d in enumerate(ds):
            if d + 10 * args.b < x:
                continue
            elif d > x + 10 * args.b:
                break
            elif d < 1e-6:
                continue
            else:
                tmp = norm * exp(-0.5*((x-d)/args.b)**2) / (4 * np.pi * d * d)
                PDF[i] += tmp
                s_PDF[ds_types[j]] += tmp

        print x,PDF[i],' '.join([str(y) for y in s_PDF])

if args.coord > 0.0:
    f = open("coord.txt","w")
    f.write("# Average coordination numbers\n")
    for t in atomtypes:
        csum = 0
        n = 0.0
        for a in neighbors:
            if a[0] == t:
                csum += a[3]
                n += 1
        f.write(t + " " + str(float(csum)/n) + "\n")
    f.write("\n")

    for at in cnumtypes:
        counts=[0.0 for i in range(10)]
        f.write("# Local environments of:" + " " + at[0] + "\n")
        for t in at[1]:
            n = 0.0
            for a in neighbors:
                if a[1] == t and a[0] == at[0]:
                    n +=1
                    counts[a[3]] += 1
            f.write(t + ":" + str(n/len(data)) + "\n")

        for i,c in enumerate(counts):
            if c > 0:
                f.write(str(i) + at[0] + ":" + str(c/len(data))  + "\n" )
        f.write("\n")
