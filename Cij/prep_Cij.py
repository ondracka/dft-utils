#!/usr/bin/env python3

import pymatgen as pmg
import pymatgen.io.vasp.inputs as inputs
import os, shutil
from sys import argv
import argparse
import re
import numpy as np
from ase import io
from ase.calculators.espresso import Espresso
from common import read_qe_settings

parser = argparse.ArgumentParser(description='Generate displacements for the stress-strain method')
parser.add_argument('-q', help='use QUANTUM Espresso', action='store_true')
parser.add_argument('-i', help='path to the input .in file')
args = parser.parse_args()

ULICs = {1: [1, -2, 3, -4, 5, -6],
         2: [2, 1, -5, -6, 4, 3],
         3: [3, 4, -1, 5, 6, -2],
         4: [4, -3 ,6, 1, -2, 5],
         5: [5, 6, 2, -3, -1, -4],
         6: [6, -5, -4, 2, -3, 1]}

maxEps = [0.007, 0.014, 0.021]

eqDir = 'eq'

#get equlibrium structures
if args.q:
    struct = io.read(args.i, format='espresso-in')
    params, pps, kpts, koffset = read_qe_settings(args.i)
    params['calculation'] = 'relax'
    print(params, pps)
    cell = np.array(struct.get_cell())
else:
    try:
        CONTCAR = inputs.Poscar.from_file(eqDir+'/CONTCAR')
        print(CONTCAR.structure.lattice.matrix)
    except:
        print('Cannot read CONTCAR from {}'.format(eqDir))
        quit()

    try:
        POTCAR = inputs.Potcar.from_file(eqDir+'/POTCAR')
    except:
        print('Cannot read POTCAR from {}'.format(eqDir))
        quit()

    try:
        INCAR = inputs.Incar.from_file(eqDir+'/INCAR')
    except:
        print('Cannot read INCAR from {}'.format(eqDir))
        quit()    
    #modify relaxation in INCAR
    INCAR['ISIF'] = 2
    INCAR['NSW'] = 150
    INCAR['IBRION'] = 2
    #keep the originals...
    #INCAR['EDIFFG'] = -1E-2
    #INCAR['EDIFF'] = 1E-6
    INCAR['LWAVE'] = False
    INCAR['LCHARG'] = False
    INCAR.pop('LORBIT', None)


def createCase(ULIC, case, eps):
    try:
        os.mkdir(case)
    except:
        print('Case {} in {} exists. Skipping...'.format(case, eps))
        return
    os.chdir(case)

    strain = np.array([[ULIC[0], ULIC[5]/2, ULIC[4]/2],
                       [ULIC[5]/2, ULIC[1], ULIC[3]/2],
                       [ULIC[4]/2, ULIC[3]/2, ULIC[2]]])
    strain = strain*eps/6  
    deformation = strain + np.eye(3)
    
    if args.q:
        struct.set_cell(np.matmul(cell, deformation), scale_atoms=True)
        calc = Espresso(command='', input_data=params, pseudopotentials=pps, kpts=kpts, koffset=koffset)
        calc.write_input(struct)

    else:
        INCAR.write_file('INCAR')
        os.system('cp ../../eq/KPOINTS .')
        POTCAR.write_file('POTCAR')
        lattice = CONTCAR.structure.lattice.matrix
        POSCAR = CONTCAR.structure.copy()
        POSCAR.lattice = pmg.Lattice(np.matmul(lattice, deformation))
        POSCAR.to(filename='POSCAR')

    os.chdir('..')
    

#prepare cases
for e in maxEps:
    eps = 'eps={}'.format(e)
    try:
        os.mkdir(eps)
    except:
        print ('Deformation {} already exists...'.format(eps))
    os.chdir(eps)
     
    for k, ULIC in ULICs.items():
        case="eps{}".format(k)
        createCase(ULIC, case, e)
        case="eps-{}".format(k)
        createCase(np.array(ULIC)*(-1), case, e)
                     
    os.chdir('..')

# vim: set expandtab tabstop=4 shiftwidth=4:
