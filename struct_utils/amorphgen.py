#!/usr/bin/env python3

from sys import argv
import argparse
import random
import numpy as np
from common import printopenmx, atrad
from ase import Atoms, Atom, io
import sys


def eprint(*largs, **kwargs):
    if args.verbose:
        print(*largs, file=sys.stderr, **kwargs)


parser = argparse.ArgumentParser(description='Create inital amorphous cell for MD simulations with selected density')
parser.add_argument('-o', help='output ase file format (default OpenMX)', type=str)
parser.add_argument('-d', help='mass density (g/cm3)', type=float)
parser.add_argument('-s', help='initial random see (for testing purposes)', type=int)
parser.add_argument('-v', help='verbose output', dest='verbose', action='store_true')
parser.add_argument("atoms", help='list of atoms in the supercell "B:10 Al:50 etc...')
args = parser.parse_args()

if args.s:
    eprint("Initiating random seed to", args.s)
    random.seed(args.s)

s = Atoms()
s.pbc = [True, True, True]

for a in args.atoms.split():
    symbol, count = a.split(':')
    for i in range(int(count)):
        s.append(Atom(symbol, (0, 0, 0)))

mass = sum(s.get_masses()) * 1.66053904e-24
Vincm = mass / args.d
a = Vincm ** (1.0 / 3.0) * 1e8

s.cell = np.array([[a, 0.0, 0.0], [0.0, a, 0.0], [0.0, 0.0, a]])

i = 0
tries = 0
maxdist = 0.9
while i < len(s):
    if tries > 100:
        maxdist = maxdist - 0.05
        eprint("reducing max dist to", maxdist)
        tries = 0
    p = np.array([random.random() * a, random.random() * a, random.random() * a])
    s[i].position = p
    eprint(s[i])
    badpos = 0
    for j in range(i):
        d = s.get_distance(i, j, mic=True)
        eprint("distance between {} and {} is {}".format(i,j,d))
        if d < (atrad(s[i].symbol) + atrad(s[j].symbol)) * maxdist:
            badpos = 1
            tries += 1
            break
    if badpos == 0:
        eprint ("generated position accepted")
        i += 1
        tries = 0

if args.o:
    io.write("-", s, format=args.o)
else:
    printopenmx(s)
