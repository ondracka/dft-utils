#!/usr/bin/env python3

import pandas as pd
from ase import Atoms,io
from ase.units import Rydberg,create_units
from numpy import sqrt

import os

per_atom_energy = {'C' : -17.76542885, 'B' : -6.00713732, 'W' : -158.30229357}

units = create_units('2006')
energy = []
forces = []
ase_atoms = []
energy_corrected = []

large_forces = 0
skip = False

for root, dirs, files in os.walk("."):
    if "2x1x2" in root:
        print("skipping")
    for file in files:
        if file != "pw.out":
            continue
         
        print("newstruct", root + "/pw.out")
        structs = io.read(root + "/pw.out", index=slice(None), format="espresso-out")

        if len(structs) == 0:
            continue

        if len(structs) != 1:
            print("Problem")
            exit()

        for f in structs[0].get_forces():
            if sqrt(f[0] * f[0] + f[1] * f[1] + f[2] * f[2]) > 50:
                print("forces too large")
                skip = True
                large_forces += 1
                break

        if skip is True:
            skip = False
            continue

        for line in open(root + "/pw.out", 'r'):
            if "internal energy" in line:
               e = float(line.split()[4]) * units['Ry']
               energy.append(e)
               for a in structs[0].get_chemical_symbols():
                   e = e - per_atom_energy[a] * units['Ry']
               #pri(s.get_total_energy(),ecor)
               energy_corrected.append(e)
               #print(e, structs[0].get_potential_energy(force_consistent=True), structs[0].get_total_energy())

        for i,s in enumerate(structs):
            #print(Rydberg, units['Ry'])
            forces.append(s.get_forces())
            s.set_calculator(calc=None)
            ase_atoms.append(s)

print(len(energy))
# set reference energy to 0
reference_energy = 0
# collect all the data into a dictionary 
data = {'energy': energy,
        'forces': forces, 
        'ase_atoms': ase_atoms, 
        'energy_corrected': energy_corrected}
# create a DataFrame
df = pd.DataFrame(data)
# and save it 
df.to_pickle('my_data.pckl.gzip', compression='gzip', protocol=4)
print(large_forces)
