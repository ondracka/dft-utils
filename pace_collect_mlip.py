#!/usr/bin/env python3

import pandas as pd
from ase import Atoms,io

import sys

energy = 0
energies = []
ase_atoms = []
cell = []
atoms = []
symbols = []
forces = []
all_forces=[]

readsize = False
readenergy = False
readcell = 0
size = 0

sym = ["W","B","C"]
        
for line in open(sys.argv[1], "r"):
    #print(line)
    if "BEGIN_CFG" in line:
        continue
    if "END_CFG" in line:

        ase_atoms.append(Atoms(symbols=symbols, positions=atoms, cell=cell, pbc=True))
        all_forces.append(forces)
        energies.append(energy)
        symbols = []
        atoms = []
        forces = []
        cell = []

        continue
    if "Size" in line:
        readsize = True;
        continue
    if readsize:
        size = int(line)
        readsize = False
        continue
    if " Energy" in line:
        readenergy = True
        continue
    if readenergy:
        energy = float(line)
        readenergy = False
        continue
    if " Supercell" in line:
        readcell = 3
        cell = []
        continue
    if readcell > 0:
        l = line.split()
        cell.append([float(l[0]), float(l[1]), float(l[2])])
        readcell = readcell - 1
        continue

    if (size <= 0):
        continue
    l = line.split()

    if "AtomData" in l[0]:
        continue

    symbols.append(sym[int(l[1])])
    atoms.append([float(l[2]), float(l[3]), float(l[4])])
    forces.append([float(l[5]), float(l[6]), float(l[7])])
    size = size - 1

# set reference energy to 0
reference_energy = 0
# collect all the data into a dictionary 
data = {'energy': energies,
        'forces': all_forces, 
        'ase_atoms': ase_atoms, 
        'energy_corrected': energies}
# create a DataFrame
df = pd.DataFrame(data)
# and save it 
df.to_pickle('my_data.pckl.gzip', compression='gzip', protocol=4)
