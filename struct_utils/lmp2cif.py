#!/usr/bin/env python3

from ase import io
final = 0
for atoms in io.iread("dump.small", format='lammps-dump-text'):
   final = atoms

for a in final:
    if a.symbol == "H":
        a.symbol = "B"
    if a.symbol == "He":
        a.symbol = "C"
    if a.symbol == "Li":
        a.symbol = "W"

io.write("final.cif", final, format='cif')
