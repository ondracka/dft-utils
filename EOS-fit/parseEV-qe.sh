#!/bin/bash

rm -f EV.txt
QEOUTDIR=$(grep "outdir[ =]*.*" 0.985/${1}.in | sed 's/outdir *= *\(.*\)/\1/g' | tr -d \'\" | tr -d ' ')
QEOUTDIR="$QEOUTDIR/*.save"
for A in 0.985 0.99 0.995 1.0 1.005 1.01 1.015
do
   TMP=$(grep '<etot>' $A/${QEOUTDIR}/*.xml | grep -Eo "[-0-9\.eE]{2,}" | tail -n1)
   ENE=$(awk '{printf "%.7f", $1 * 27.2113825435}' <<< "$TMP")
   TMP=$(grep -A3 '<cell>' $A/${QEOUTDIR}/*.xml | tail -n3 | tr -d '\n' | sed 's/<\/*a[0-9]>//g')
   VOL=$(awk '{print (($2*$6-$3*$5)*$7 + ($3*$4-$1*$6)*$8 + ($1*$5-$2*$4)*$9)*0.148184709}' <<< "$TMP")
   echo "$VOL $ENE" >> EV.txt
   if [ "$A" == "1.0" ]
   then
      cp $HOME/dft-utils/EOS-fit/BM-EOS.gpl .
      sed -i "s/E0 = .*/E0 = $ENE/g" BM-EOS.gpl
      sed -i "s/V0 = .*/V0 = $VOL/g" BM-EOS.gpl
   fi
done
TMP=$(gnuplot BM-EOS.gpl 2>&1 >/dev/null)
grep -A4 "Final set of parameters" <<< "$TMP" | tail -n1 | awk '{print $3,$5}'

