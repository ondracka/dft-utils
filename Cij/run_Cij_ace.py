#!/usr/bin/env python3

import os, shutil
import argparse
import re
import numpy as np
from ase import io
from ase.filters import UnitCellFilter
from ase.optimize import BFGS
from pyace import PyACECalculator
from numpy.linalg import inv, pinv
from elastic import *

parser = argparse.ArgumentParser(description='Calculate mechanical properties with ASE potential')
parser.add_argument("-e", help="maximum displacement", type=float)
parser.add_argument("yaml_potential_file")
parser.add_argument("structure_file")
args = parser.parse_args()

ULICs = {1: [1, -2, 3, -4, 5, -6],
         2: [2, 1, -5, -6, 4, 3],
         3: [3, 4, -1, 5, 6, -2],
         4: [4, -3 ,6, 1, -2, 5],
         5: [5, 6, 2, -3, -1, -4],
         6: [6, -5, -4, 2, -3, 1]}

maxEps = [0.007, 0.014, 0.021]

struct = io.read(args.structure_file)
print("intiial structure:", struct)
cell = np.array(struct.get_cell())

calc = PyACECalculator('output_potential.yaml')
#calc.set_active_set("output_potential.asi")

#now relax it to get the equilibrium cell
struct.calc = calc
struct_relax = UnitCellFilter(struct)
# Evaluate properties
dyn = BFGS(struct_relax)
dyn.run(fmax=0.00001, steps=1000)
eq_matrix = np.array(struct.get_cell())

def runCase(ULIC, eps, s):
    #print(ULIC, eps, s)
    strain = np.array([[ULIC[0], ULIC[5]/2, ULIC[4]/2],
                       [ULIC[5]/2, ULIC[1], ULIC[3]/2],
                       [ULIC[4]/2, ULIC[3]/2, ULIC[2]]])
    strain = strain*eps/6  
    deformation = strain + np.eye(3)
    
    s.set_cell(np.matmul(cell, deformation), scale_atoms=True)
    #print(s)

    dyn = BFGS(s)
    dyn.run(fmax=0.00001, steps=1000)
    #print(s)
    return s.get_stress() * -160.21766208, s.get_cell()


#prepare cases
e = args.e
strains = []
stresses = []

for k, ULIC in ULICs.items():
    for i in [1, -1]:
        print(i)
        new_struct = struct.copy()
        new_struct.calc = calc
        stress, case_lattice_mat = runCase(np.array(ULIC)*i, e, new_struct)
                     
        deformation_mat = np.matmul(inv(eq_matrix), case_lattice_mat) - np.identity(3)
        deformation_vec = np.array([deformation_mat[0,0],
                                    deformation_mat[1,1],
                                    deformation_mat[2,2],
                                    2*deformation_mat[1,2],
                                    2*deformation_mat[0,2],
                                    2*deformation_mat[0,1]])
        print(deformation_mat)
        strains.append(deformation_vec)
        stresses.append(stress)

print(stresses)

epsilon = np.array(strains)
sigma = np.array(stresses)

if epsilon.shape == sigma.shape:
    Cij = np.matmul(pinv(epsilon), sigma)
    Cij = -0.5 * (Cij + Cij.T)

    el = Elastic(str(Cij).replace('\n', '').replace(' [', '').replace('[', '').replace(']', '\n'))
    averages = el.averages()

    eigenval = sorted(np.linalg.eig(el.CVoigt)[0])
    if eigenval[0] < 0.0:
        print("Warning: Is not mechanically stable!")
    print("Hills K: " + str(averages[2][0]))
    print("Hills E: " + str(averages[2][1]))
    print("Hills G: " + str(averages[2][2]))
    print("Hills gamma: " + str(averages[2][3]))

# vim: set expandtab tabstop=4 shiftwidth=4:
