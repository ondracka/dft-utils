#!/usr/bin/env python3

from sys import argv
import argparse
from common import printopenmx
from ase import io

parser = argparse.ArgumentParser(description='Convert VASP CONTCAR/POSCAR files into OpenMX structure files')
parser.add_argument("input_file", help='path to the CONTCAR file')
args = parser.parse_args()

s = io.read(args.input_file, format="vasp")
printopenmx(s)
