#!/usr/bin/env python3

from ase import io, neighborlist
import sys
import numpy as np

atoms = io.read(sys.argv[1], format='espresso-out')
nat = len(atoms)

cutoffs = neighborlist.natural_cutoffs(atoms, mult=1)
cutoffs = {('Ta', 'Ta'): 3.0, ('Ta', 'O'): 2.6, ('Al', 'O'): 2.3, ('O', 'O'): 2.0}

nlist = neighborlist.neighbor_list(('i','j'), atoms, cutoffs)
#print(nlist)
ncounts = np.bincount(nlist[0])

elems = {a.symbol for a in atoms}
ncountselem={s:[] for s in elems}

for e in elems:
    for i in range(nat):
        if atoms[i].symbol == e:
            ncountselem[e].append(ncounts[i])

#print(ncountselem)

for e in elems:
    print(e, np.average(ncountselem[e]))
