#!/bin/bash

rm -f EV.txt
for A in 0.985 0.99 0.995 1.0 1.005 1.01 1.015
do
   ENE=$(cat $A/OUTCAR | grep "energy without" | tail -n1 | awk '{print $8}')
   VOL=$(cat $A/OUTCAR | grep "volume" | tail -n1 | awk '{print $5}')
   echo "$VOL $ENE" >> EV.txt
   if [ "$A" == "1.0" ]
   then
      cp $MYWORKDIR/EOS-fit/BM-EOS.gpl .
      sed -i "s/E0 = .*/E0 = $ENE/g" BM-EOS.gpl
      sed -i "s/V0 = .*/V0 = $VOL/g" BM-EOS.gpl
   fi
done
TMP=$(gnuplot BM-EOS.gpl 2>&1 >/dev/null)
grep -A4 "Final set of parameters" <<< "$TMP" | tail -n1 | awk '{print $3,$5}'
$MYWORKDIR/EOS-fit/nomad.py
