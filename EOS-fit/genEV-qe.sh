#!/bin/bash

for A in 0.985 0.99 0.995 1.0 1.005 1.01 1.015
do
   mkdir $A
   cp -r *.in $A/
   cd "$A"
   sed -i "s/cell_dofree.*/cell_dofree = 'shape'/g" *.in
   LATTICE=$(grep -A3 "CELL_PARAMETERS" *.in | tail -n3)
   CARD=$(grep "CELL_PARAMETERS" *.in)
   sed -i '/CELL_PARAMETERS/,+3d' *.in
   echo "$CARD" >> *.in
   awk -v a=$A '{printf "%.10f %.10f %.10f\n", $1*a, $2*a, $3*a}' <<< "$LATTICE" >> *.in
   $1 $2
   cd ".."
done
