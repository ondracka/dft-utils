#!/bin/bash

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright (c) 2016-2020 Pavel Ondračka.

BOHR2ANG=0.529177249

function help {
   echo "Script to parse output from convergence run"
   echo ""
   echo "Usage: conv_parse [option]"
   echo ""
   echo "Supported checks"
   echo "-f forces"
   echo "-c CEBEs"
   echo "-C [ij] Cij matrix elements"
	echo "-e total energy"
	echo "-E [symmetry type = c,h,t,...] elastic modulus"
	echo "-V unit cell volume"
	echo "-l [type = c,h,t,...] lattice parameters"
   echo "-s stress tensor"
   echo "-g band gap"
   echo "-b bulk modulus (from BM scripts)"
	echo "-T time"
   echo "-h print this help and exit"
   echo "-v output more information"
	echo "-t [output plot title]"
}

debug_echo () {
	if [ "$DEBUG" == 1 ]
	then
		echo "$1" 1>&2
	fi
}

get_natoms () {
   if [ "$CODE" == "VASP" ]
   then
		NAT=$(awk 'BEGIN{s = 0} {if(NR == 7){s = $1+$2+$3+$4+$5+$6+$7}} END{print s}' $1/POSCAR)
   elif [ "$CODE" == "OpenMX" ]
   then
   	NAT=$(grep -A 1 "<coordinates.forces" $1/*.out | tail -n1)
   elif [ "$CODE" == "qe" ]
   then
		NAT=$(grep -o "nat=\"[0-9]*\"" ${1}/${QEOUTDIR}/*.xml | grep -o "[0-9]*" | tail -n1)
   fi
}

#FIXME: hardcoded filenames
#FIXME: error properly if values not found
energy () {
	debug_echo "Parsing energy value in $1"
	if [ "$CODE" == "Wien2k" ]
	then
		grep "\:ENE" $1/*.scfm | awk '{printf "%.10f\n", $9 * 13.606}'
	elif [ "$CODE" == "OpenMX" ]
	then
		awk '{printf "%.7f\n", $15 * 13.606 * 2}' $1/*.ene
	elif [ "$CODE" == "VASP" ]
	then
      get_natoms $1
		grep TOTEN $1/OUTCAR | tail -n 1 | awk -v n=$NAT '{print $5/n}'
	elif [ "$CODE" == "qe" ]
	then
		debug_echo "looking for total energy values in ${1}/${QEOUTDIR}/*.xml"
		TMP=$(grep '<etot>' ${1}/${QEOUTDIR}/*.xml | grep -Eo "[-0-9\.E+]+" | tail -n1)
		echo $(awk '{printf "%.7f", $1 * 27.2113825435}' <<< "$TMP")
	else
		debug_echo "energy: unsupported code!"
		exit 3
	fi
}

gap () {
	debug_echo "Parsing gap value in $1"
	if [ "$CODE" == "qe" ]
	then
		debug_echo "looking for gap values in ${1}/pw.out"
		TMP=$(grep 'highest occupied' ${1}/pw.out | grep -Eo "[-0-9\. ]*" | tail -n1)
		echo $(awk '{printf "%.6f", $2 - $1}' <<< "$TMP")
	else
		debug_echo "energy: unsupported code!"
		exit 3
	fi
}

runtime () {
	debug_echo "Parsing runtime value in $1"
	if [ "$CODE" == "qe" ]
	then
		debug_echo "looking for runtime values in ${1}/pw.out"
		TMP=$(grep -o "PWSCF.*CPU.*WALL" ${1}/pw.out | sed 's/.*CPU\(.*\)WALL/\1/g')
		debug_echo "$TMP"
		SEC=$(grep -o ".\{5\}s" <<< "$TMP")
		SEC=${SEC:0:2}
		MIN=$(grep -o ".\{2\}m" <<< "$TMP")
		MIN=${MIN:0:2}
		HOUR=$(grep "h" <<< "$TMP" | sed 's/.*\([0-9]\{1,\}\)h.*/\1/g')
		if [ "$SEC" == "" ]
		then
			SEC=0
		fi
		if [ "$MIN" == "" ]
		then
			MIN=0
		fi
		if [ "$HOUR" == "" ]
		then
			HOUR=0
		fi
		debug_echo "${HOUR}h ${MIN}m ${SEC}s"
		awk "{print $SEC + $MIN * 60 + $HOUR * 3600}" <<< ""
	else
		debug_echo "energy: unsupported code!"
		exit 3
	fi
}

#FIXME: generalize
CEBE () {
	debug_echo "Parsing XPS value in $1"
	if [ "$CODE" == "Wien2k" ]
	then
		awk '{print $6 * 13.606}' "$1/XPS-shifts-dscf-cond.txt"
	else
		debug_echo "CEBE: unsupported code!"
		exit 3
	fi
}

lattice () {
	debug_echo "latice: extracting from $1"
   if [ "$CODE" == "VASP" ]
   then
		TMP=$(grep -A 3 "direct lattice vectors" "$1/OUTCAR" | tail -n 3)
		a=$(awk '{if (NR==1) print sqrt($1*$1 + $2*$2 + $3*$3)}' <<< "$TMP")
		b=$(awk '{if (NR==2) print sqrt($1*$1 + $2*$2 + $3*$3)}' <<< "$TMP")
		c=$(awk '{if (NR==3) print sqrt($1*$1 + $2*$2 + $3*$3)}' <<< "$TMP")
   elif [ "$CODE" == "OpenMX" ]
   then
		a=$(grep "^ a1 = " "$1/abc.out" | awk '{print $3}')
		b=$(grep "^ a2 = " "$1/abc.out" | awk '{print $4}')
		c=$(grep "^ a3 = " "$1/abc.out" | awk '{print $5}')
   elif [ "$CODE" == "qe" ]
   then
		TMP=$(grep -A3 "<cell>" ${1}/${QEOUTDIR}/*.xml | tail -n3 | tr -d "\n" | sed 's/<\/*a[0-9]>//g')
		a=$(awk "{print \$1 * $BOHR2ANG}" <<< "$TMP")
		b=$(awk "{print \$5 * $BOHR2ANG}" <<< "$TMP")
		c=$(awk "{print \$9 * $BOHR2ANG}" <<< "$TMP")
	else
		debug_echo "lattice: unsupported code"
		exit 1
	fi
	if [ "$LATTICE" == "h" ]
	then
		echo "$a $c"
		echo "$a $c" > tmpa
	else
		echo "$a $b $c"
		echo "$a $b $c" > tmpa
	fi
	debug_echo "lattice: $1 $a $b $c"
}

forces () {
   if [ "$CODE" == "OpenMX" ]
   then
   	NAT=$(grep -A 1 "<coordinates.forces" $1/*.out | tail -n1)
		grep -A $NAT "<coordinates.forces" $1/*.out | tail -n +3 > tmpcur
		grep -A $NAT "<coordinates.forces" $BESTDIR/*.out | tail -n +3 > tmpbest
		join tmpcur tmpbest > tmpall
		awk 'BEGIN {sum = 0; max = 0; sumdiff = 0; maxdiff = 0}; \
			  {fcur = sqrt(($6)^2 + ($7)^2 + ($8)^2) * 51.422; \
			  fdiff = sqrt(($6-$13)^2 + ($7-$14)^2 + ($8-$15)^2) * 51.422; \
			  sum += fcur; \
			  sumdiff += fdiff; \
			  if(fcur > max) {max = fcur}; if(fdiff > maxdiff) {maxdiff = fdiff}} \
			  END { print sum/NR/3, sumdiff/NR/3, max, maxdiff}' tmpall
		rm tmpcur tmpbest tmpall
	elif [ "$CODE" == "qe" ]
	then
		NAT=$(grep -o "nat=\"[0-9]*\"" ${1}/${QEOUTDIR}/*.xml | grep -o "[0-9]*" | tail -n1)
		grep -A$NAT "<forces " ${1}/${QEOUTDIR}/*.xml | tail -n$NAT > tmpcur
		grep -A$NAT "<forces " $BESTDIR/${QEOUTDIR}/*.xml | tail -n$NAT > tmpbest
	   paste tmpcur tmpbest > tmpall
		awk 'BEGIN {sum = 0; max = 0; sumdiff = 0; maxdiff = 0}; \
			  {fcur = sqrt($1^2 + $2^2 + $3^2); \
			  fbest = sqrt($4^2 + $5^2 + $6^2); \
		  	  fdiff = sqrt(($1 - $4)^2 + ($2 - $5)^2 + ($3 - $6)^2); \
		     sum += fcur; \
		     sumdiff += fdiff; \
           if(fcur > max) {max = fcur}; if(fdiff > maxdiff) {maxdiff = fdiff}} \
		     END { print sum/NR * 51.422, sumdiff/NR * 51.422, max * 51.422, maxdiff * 51.422}' tmpall
		rm tmpcur tmpbest tmpall
	elif [ "$CODE" == "VASP" ]
	then
      get_natoms $1
		grep -A$(($NAT+1)) "TOTAL-FORCE" ${1}/OUTCAR | tail -n$NAT > tmpcur${1}
		grep -A$(($NAT+1)) "TOTAL-FORCE" ${BESTDIR}/OUTCAR | tail -n$NAT > tmpbest${1}
	   paste tmpcur${1} tmpbest${1} > tmpall
		awk 'BEGIN {sum = 0; max = 0; sumdiff = 0; maxdiff = 0}; \
			  {fcur = sqrt($4^2 + $5^2 + $6^2); \
			  fbest = sqrt($10^2 + $11^2 + $12^2); \
		  	  fdiff = sqrt(($4 - $10)^2 + ($5 - $11)^2 + ($6 - $12)^2); \
		     sum += fcur; \
		     sumdiff += fdiff; \
           if(fcur > max) {max = fcur}; if(fdiff > maxdiff) {maxdiff = fdiff}} \
		     END { print sum/NR, sumdiff/NR, max, maxdiff}' tmpall
		#rm tmpcur tmpbest tmpall
	else
      echo "Unsupported code"
      exit 7
   fi
}

stress () {
	if [ "$CODE" == "VASP" ]
	then
		grep "in kB" $i/OUTCAR | tail -n1 | awk '{print $3*0.1, $4*0.1, $5*0.1}'
	elif [ "$CODE" == "qe" ]
	then
		grep -A3 "<stress rank=" ${1}/${QEOUTDIR}/*.xml | tail -n3 | tr -d '\n' | awk '{print $1*29421, $5*29421, $9*29421}'
	elif [ "$CODE" == "OpenMX" ]
	then
		#FIXME: convert to normal units!!!
	   #cellvol=$(grep "Cell_Volume" "$1/TiON.stdout" | tail -n1 | awk '{print $3}')
	   #volinm=$(awk '{printf "%.4e", $1 * 5.291e-11 * 5.291e-11 * 5.291e-11}' <<< $cellvol)
	   TMP=$(grep "Stress tensor (Hartree/bohr^3)" -A5 "$1/TiON.stdout" | tail -n 3)
		a=$(awk '{if (NR == 1) {print $1}}' <<< "$TMP")
		b=$(awk '{if (NR == 2) {print $2}}' <<< "$TMP")
		c=$(awk '{if (NR == 3) {print $2}}' <<< "$TMP")
	   #sa=$(awk '{print $1 * 4.359e-18 / 5.291e-11 / 1e9}' <<< "$a $volinm")
	   #sb=$(awk '{print $1 * 4.359e-18 / 5.291e-11 / $2 / 1e9}' <<< "$b $volinm")
	   #sc=$(awk '{print $1 * 4.359e-18 / 5.291e-11 / $2 / 1e9}' <<< "$c $volinm")
		echo "$a $b $c"
		echo "$sa $sb $sc" > tmp
   else
      echo "Unsupported code"
      exit 7
   fi
}

parse_Cij () {
	if [ ! -f "$i/stress/Cij-all.dat" ]
	then
		cd $i/stress
		#FIXME: hardcoded paths
		python3 ~/work/DFT/Cij/calculate_Cij.py > Cij-all.dat
		#FIXME: hardcoded displacements
		for eps in 0.007 0.014 0.021
		do
			if grep "eps=$eps" Cij-all.dat
			then
				grep "eps=$eps" -A 6 Cij-all.dat | tail -n6 | sed 's/\[//g' | sed 's/\]//g' > Cij-$eps.dat
			fi
		done		
		cd ../..
	fi
}

CIJ () {
   if [ "$CODE" == "VASP" ]
   then
		parse_Cij
		echo "$CIJTYPE" >> CIIII
		CI=${CIJTYPE:0:1}
		CJ=${CIJTYPE:1:1}
		TMP=""
		#FIXME: hardcoded displacements
		for eps in 0.007 0.014 0.021
		do
			if [ -f "$i/stress/Cij-${eps}.dat" ]
			then
				TMP="$TMP $(awk -v y=$CJ "{if(NR == $CI) printf \$y}" $i/stress/Cij-${eps}.dat)"
			fi
		done
		debug_echo "CIJ: parsed value(s) of C${CI}${CJ} $TMP"
		echo "$TMP"
	fi
}

elmod () {
   if [ "$CODE" == "VASP" ]
   then
		parse_Cij
		cd $i/stress
		TMP=""

		for FILE in Cij-*.dat
      do
			cp $FILE Cij.dat
			if [ "$LATTICE" == "h" ]
			then
				#FIXME: hardcoded paths
				python3 ~/work/DFT/Cij/project_Cij_hexagonal.py &> /dev/null
				TMP="$TMP $(bash ~/work/DFT/Cij/calc_EGB_Cij_hex.sh Cij_hexagonal.dat)"
			elif [ "$LATTICE" == "o" ]
			then
				#FIXME: hardcoded paths
				python3 ~/work/DFT/Cij/project_Cij_orthorhombic.py &> /dev/null
				TMP="$TMP $(bash ~/work/DFT/Cij/calc_EGB_Cij_ortho.sh Cij_ortho.dat)"
			elif [ "$LATTICE" == "t" ]
			then
				echo FIXME unsupported lattice
				exit
			elif [ "$LATTICE" == "c" ]
			then
				#FIXME: hardcoded paths
				python3 ~/work/DFT/Cij/project_Cij_cubic.py &> /dev/null
				TMP="$TMP $(bash ~/work/DFT/Cij/calc_EGB_Cij_cubic.sh Cij_cubic.dat)"
			else
				echo FIXME unsupported lattice $LATTICE
      	   exit
			fi
			rm Cij.dat

		done

		echo "$TMP"
	
		cd ../..
	fi
}

bulk_modulus () {
   if [ "$CODE" == "VASP" ]
   then
      cd $1
      $MYWORKDIR/DFT/EOS-fit/parseEV-VASP.sh
      cd ..
   elif [ "$CODE" == "qe" ]
   then
      cd $1
      $MYWORKDIR/DFT/EOS-fit/parseEV-qe.sh
      cd ..
   else
      echo "Unsupported code"
		exit 7
   fi
}

volume () {
   if [ "$CODE" == "VASP" ]
   then
		TMP=$(grep -A 3 "direct lattice vectors" "$1/OUTCAR" | tail -n 3)
		awk '{if (NR == 1) {a = $1; b = $2; c = $3};
		     if (NR == 2) {d = $1; e = $2; f = $3};
		     if (NR == 3) {g = $1; h = $2; i = $3}}
           END{ print a*(e*i - f*h) - b * (d*i - f*g) + c* (d*h - e*g)}
		' <<< "$TMP"
		# this doesn't have enough digits
      #grep "volume of cell" $1/OUTCAR | tail -n1 | awk '{print $NF}'
   else
      echo "Unsupported code"
		exit 1
   fi
}

getvalue () {
	debug_echo "getvalue: $1"
	VAL=$(tr '_' '\n' <<< $1 | tail -n1)
	#forceconv special treatment
	if [ "$t" == "forceconv" ]
	then
		#FIXME, this will not work properly for positive settings
		VAL=$(awk '{if ($1 < 0) print -$1; else print $1}' <<< "$VAL") 
	# Kpoints special treatment
	elif [ "$t" == "kpoint" ]
   then
		if grep -q "x" <<< $VAL
		then
			TMP=($(tr 'x' "\n" <<< $VAL))
			VAL=$((TMP[0] * TMP[1] * TMP[2]))
		fi
	fi

	echo "$VAL"
	debug_echo "getvalue: $VAL"
}

findbest () {
	if [ "$1" == "kpoint" ] || [ "$1" == "cutoff" ] || [ "$1" == "orderN-hoprange" ] || [ "$1" == "orderN-krylovH" ] || [ "$1" == "orderN-numhop" ] || \
		[ "$1" == "RKmax" ] || [ "$1" == "lvns" ] || [ "$1" == "Gmax" ] || [ "$1" == "super" ] || [ "$1" == "ispin" ] || [ $1 == "bands" ] 
	then
		OP=">"
		BEST="1e-100"
	elif [ "$1" == "eneconv" ] || [ "$1" == "forceconv" ] || [ "$1" == "smearing" ]
	then
		OP="<"
		BEST="1e100"
	else
		echo "findbest: Unhandled convergence parameter $1"
		exit
	fi

	j=0
	MAXNAME=""
	for i in $1_*
	do 
		BESTOLD=$BEST
		VAL=$(getvalue "$i")
		BEST=$(echo "$VAL $BEST" | awk "{if (\$1 $OP \$2) {print \$1} else {print \$2}}")
		debug_echo "findmax: $BEST $BESTOLD"
		if [ "$BEST" != "$BESTOLD" ]
		then
			BESTDIR=$i
		fi
	done

	debug_echo "findbest: $BESTDIR"
}

find_types () {
	SUPPORTED="kpoint cutoff eneconv orderN-hoprange orderN-numhop orderN-krylovH RKmax lvns Gmax super ispin forceconv bands smearing"

	for t in $SUPPORTED
	do
	   debug_echo "find_types: checking for $t"
	   if ls -d */ | grep -q "$t"
		then
			TYPES="$TYPES $t"
		fi
	done
	
	NTYPES=$(wc -w <<< "$TYPES")
	
	debug_echo "find_types: found $NTYPES: $TYPES"
}

detect_code () {
	debug_echo "Checking what DFT sofware are we running"

   if [ -f INCAR ] && [ -f POSCAR ] && [ -f POTCAR ] && [ -f KPOINTS ]
   then
      CODE="VASP"
		debug_echo "VASP detected"
	elif [ -f  "$(basename $PWD).struct" ]
	then
		CODE="Wien2k"
		debug_echo "Wien2k detected"
	elif grep -q "Atoms.SpeciesAndCoordinates" *.dat 
	then
		CODE="OpenMX"
		debug_echo "OpenMX detected"
	elif [ $(grep '&SYSTEM' *.*) ]
	then
		CODE="qe"
		debug_echo "Quantum ESPRESSO detected"
		QEOUTDIR=$(grep "outdir[ =]*.*" *.in | sed 's/outdir *= *\(.*\)/\1/g' | tr -d \'\" | tr -d ' ')
		QEOUTDIR="$QEOUTDIR/*.save"
		if [ "QEOUTDIR" == "" ]
		then
			debug_echo "Unable to find QE directory"
			exit 31
		fi
		echo XXX $QEOUTDIR
	else
		echo "Unable to autodotect code"
		exit
   fi
}

key_location () {
	# blindly guess the best location for gnuplot key
	# values at higher parameters are more likely to be converged to the best guess for key positions
	# would be right and than just check if the last value is closer to top or bottom
	#FIXME: this works now just for linear scale
	#FIXME: also probably broken with NLINES != 1
	TMP=$1
	awk -v best=$BEST -v col=$((TMP + 1)) '{ if(NR == 1){}
		else if(NR == 2){min = $col; max=$col}
		else {if($col < min) min=$col; if ($col > max) max=$col; act=$col}
		if($1 == best) bval=$col}
		END{if ((max - bval) > (bval - min)){ print "top"} else{ print "bottom" }}
	' $OUTFILE
}

make_plot_header () {

	read -r -d '' HEADER << EOM
set term pdfcairo enhanced size WIDTHcm,HEIGHTcm font "Liberation Sans, 14" lw 1.5

set key samplen 0.2 box

set title font "Liberation Sans, 18" 

set multiplot layout ROWS,COLS title "MAINTITLE" font "Liberation Sans Bold, 20"

set key autotitle columnheader

set pointsize 0.7
EOM
	NPLOTS=$((NTYPES * NPROP + 1))
	debug_echo "make_plot_header: number of plots $NPLOTS"

	ROWS=$(( (NPLOTS + 1) / 2 ))
	HEIGHT=$(( ROWS * 5 + 1 ))
	if ((NPLOTS > 1))
	then
		COLS=2
	else
		COLS=1
	fi
	WIDTH=$(( COLS * 8 + 1 ))
	debug_echo "make_plot_header: rows=$ROWS cols=$COLS"
	debug_echo "make_plot_header: width=$WIDTH height=$HEIGHT"

	HEADER=$(sed "s/WIDTH/$WIDTH/g" <<< "$HEADER")
	HEADER=$(sed "s/HEIGHT/$HEIGHT/g" <<< "$HEADER")
	HEADER=$(sed "s/COLS/$COLS/g" <<< "$HEADER")
	HEADER=$(sed "s/ROWS/$ROWS/g" <<< "$HEADER")
	HEADER=$(sed "s/MAINTITLE/$MAINTITLE/g" <<< "$HEADER")

	echo "$HEADER" > conv_results/plot.gpi
}

subplot_format () {
	debug_echo "subplot_format: starting"
	XLOGSCALE=0

	if [ "$1" == "kpoint" ]
	then
		XLABEL="Number of k-points"
		XLOGSCALE=1
	elif [ "$1" == "cutoff" ]
	then
      if [ "$CODE" == "VASP" ]
      then
			XLABEL="Planewave energy cutoff (eV)"
      elif [ "$CODE" == "qe" ]
      then
			XLABEL="Planewave energy cutoff (Ry)"
		elif [ "$CODE" == "OpenMX" ]
		then
			XLABEL="Energy cutoff (Ry)"
		fi
	elif [ "$1" == "eneconv" ]
	then
		if [ "$CODE" == "qe" ]
      then
			XLABEL="Energy convergence criterion (Ry)"
		else
			XLABEL="Energy convergence criterion (Hartree)"
		fi
		XLOGSCALE=1
		XREVERSE=1
	elif [ "$1" == "smearing" ]
	then
		if [ "$CODE" == "qe" ]
      then
			XLABEL="Smearing width (Ry)"
		else
			XLABEL="Smearing width (eV)"
		fi
		XLOGSCALE=1
		XREVERSE=1
	elif [ "$1" == "forceconv" ]
	then
		XLABEL="Maximum force convergence criterion (eV/Å)"
		XLOGSCALE=1
		XREVERSE=1
	elif [ "$1" == "orderN-krylovH" ]
	then
		XLABEL="OrderN: order of the Krylov hamiltonian"
	elif [ "$1" == "orderN-numhop" ]
	then
		XLABEL="OrderN: Number of hoppings"
	elif [ "$1" == "orderN-hoprange" ]
	then
		XLABEL="OrderN: hopping range"
	elif [ "$1" == "RKmax" ]
	then
		XLABEL="R_mt * K_{max}"
	elif [ "$1" == "Gmax" ]
	then
		if [ "$CODE" == "qe" ]
		then
			XLABEL="G_{max}"
		else
			XLABEL="Charge density cutoff (Ry)"
		fi
	elif [ "$1" == "lvns" ]
	then
		XLABEL="lvns"
	elif [ "$1" == "super" ]
	then
		XLABEL="Supercell size"
	elif [ "$1" == "ispin" ]
	then
		XLABEL="Spin polarization"
	elif [ "$1" == "bands" ]
	then
		XLABEL="Number of bands"
	else
		echo "subplot_format: Unsupported convergence parameter $1"
		exit 2
	fi
	debug_echo "subplot_format: done"
}

parse_vasp_settings () {
	IMPORTANT="(^ISMEAR|^EDIFF|^EDIFFG|^SIGMA|^NSW|^IBRION|^ISPIN|^ISIF|^PREC|^ENCUT)"
	grep -E $(echo $IMPORTANT) INCAR > conv_results/settings
	printf "KPOINTS = " >> conv_results/settings
	sed -n 4p KPOINTS >> conv_results/settings

	#echo POSCAR > conv_results/structure
	#awk '{if (NF == 0) exit; else if (NR == 1); else print}' POSCAR >> conv_results/structure
}

parse_openmx_settings () {
	IMPORTANT="(^scf.XcType|^scf.SpinPolarization|^scf.Kgrid|^scf.criterion|^scf.energycutoff)"
	grep -E $(echo $IMPORTANT) *.dat > conv_results/settings
}

add_settings () {
	#function that imports the default important settings for the convergence tests
	debug_echo "add_settings: starting"
	#make a fake empty plot and dump the input there
	echo "unset border" >> conv_results/plot.gpi
	echo "unset xtics" >> conv_results/plot.gpi
	echo "unset ytics" >> conv_results/plot.gpi
	echo "set title \"Default settings\"" >> conv_results/plot.gpi
	if [ "$CODE" == "VASP" ]
	then
		parse_vasp_settings
		echo "INCAR = system('cat conv_results/settings')" >> conv_results/plot.gpi
		echo "set label 1 INCAR at graph 0.2,1.0" >> conv_results/plot.gpi
	elif [ "$CODE" == "OpenMX" ]
	then
		parse_openmx_settings
		echo "INCAR = system('cat conv_results/settings')" >> conv_results/plot.gpi
		echo "set label 1 INCAR at graph 0.2,1.0" >> conv_results/plot.gpi
	fi
	echo "plot [0:1][2:3] 1 t ''" >> conv_results/plot.gpi
	echo "set xtics auto" >> conv_results/plot.gpi
	echo "set ytics auto" >> conv_results/plot.gpi
	echo "set border" >> conv_results/plot.gpi
	echo "unset title" >> conv_results/plot.gpi
	echo "unset label 1" >> conv_results/plot.gpi
}

add_subplot () {

	debug_echo "add_subplot: $1 $CHECK"
	debug_echo "add_subplot: NLINE=$NLINE NPROP=$NPROP"

	subplot_format $1

	echo "set xlabel \"$XLABEL\"" >> conv_results/plot.gpi
	if [ "$XLOGSCALE" == "1" ]
	then
		echo "set logscale x" >> conv_results/plot.gpi
		echo "set format x '%.0e'" >> conv_results/plot.gpi
	else
		echo "unset logscale x" >> conv_results/plot.gpi
		echo "set format x '%g'" >> conv_results/plot.gpi
	fi

	#specific xlabels
	if [ "$1" == "ispin" ]
	then
		echo "set xrange [0.9:2.1]" >> conv_results/plot.gpi
		echo "set xtics (\"off\" 1, \"on\" 2)" >> conv_results/plot.gpi
	else
      echo "set xrange [*:*]" >> conv_results/plot.gpi
      echo "set xtics auto" >> conv_results/plot.gpi
	fi

	# y log scale
	if [ "${YLOGSCALE[$NUM]}" == "1" ]
	then
		echo "set logscale y" >> conv_results/plot.gpi
		echo "set format y '%.0e'" >> conv_results/plot.gpi
	else
		echo "unset logscale y" >> conv_results/plot.gpi
		echo "set format y '%g'" >> conv_results/plot.gpi
	fi

	#better precision for energy
	#FIXME: do this in a smarter way
	if [ "$CHECK" == "energy" ]
	then
		echo "set format y '%.5f'" >> conv_results/plot.gpi
	else
		echo "set format y '%g'" >> conv_results/plot.gpi
	fi

	if [ "$XREVERSE" == "1" ]
	then
		echo "set xrange [:] reverse" >> conv_results/plot.gpi
	else
		echo "unset xrange" >> conv_results/plot.gpi
	fi

	echo "$PLOTLINE" >> conv_results/plot.gpi

	J=1
	while (( J <= NPROP ))
	do
		YLABEL=$(grep -P -o '".*?"' "conv_results/${1}-${CHECK}${SUF}.txt" | sed -n $((NPROP * NLINE + 2))p)
  		echo "set ylabel $YLABEL" >> conv_results/plot.gpi
		echo "set key right $(key_location $J)" >> conv_results/plot.gpi
		printf "plot " >> conv_results/plot.gpi
		K=$((J + 1))
		while ((K < J + NLINE + 1))
		do
			TITLE=$(grep -P -o '".*?"' "conv_results/${1}-${CHECK}${SUF}.txt" | sed -n ${K}p)
			echo "'conv_results/${1}-${CHECK}${SUF}.txt' u 1:$K t $TITLE pt $((K * 2 + 1)), \\" >> conv_results/plot.gpi
			debug_echo "add_subplot: plotting column $K from file ${1}-${CHECK}${SUF}.txt with title $TITLE"
			K=$((K + 1))
		done
		echo "" >> conv_results/plot.gpi
		J=$((J + NLINE))
	done
   debug_echo "add_subplot: done"
}

# this is needed as we don't know at the time of generating the file header the specific number of epsilons calculated
detect_epsilons () {
	debug_echo "detect_epsilons: $1"
	DIR=$(find . -maxdepth 1 -type d -name "${1}_*" | tail -n1)
	debug_echo "detect_epsilons: testing in dir $DIR"
	EPSDIRSTMP=$(echo $DIR/stress/eps* | grep eps | grep -o "eps=0\.[ 0-9]*")
	EPSDIRS=""
	for D in $EPSDIRSTMP
	do
		EPSDIRS="$EPSDIRS \"$D\""
	done
	NLINE=$(wc -w <<< "$EPSDIRS")
	if ((NLINE == 0))
	then
		echo "Error: no displacements dirs detected! Check that the calculations are OK!"
		exit
	fi
	debug_echo "detect_epsilons: found $NLINE displacements dirs: $EPSDIRS"
}

make_file_header () {
	debug_echo "make_file_header: $1 $CHECK"
	subplot_format $1
	printf "\"$XLABEL\" " >> $OUTFILE
	if [ "$CHECK" == "forces" ]
	then
		echo "\"Average force\" \"Average force diff.\" \"Maximum force\" \"Maximum force diff.\" \"Forces (eV/Å)\"" >> $OUTFILE
	elif [ "$CHECK" == "energy" ]
	then
		echo "\"Total energy per atom (eV)\" \"Total energy per atom (eV)\"" >> $OUTFILE
	elif [ "$CHECK" == "CEBE" ]
	then
		echo "\"CEBE (eV)\" \"CEBE (eV)\"" >> $OUTFILE
	elif [ "$CHECK" == "bulk_modulus" ]
	then
		echo "\"Bulk modulus (GPa)\" \"Bulk modulus (GPa)\"" >> $OUTFILE
	elif [ "$CHECK" == "volume" ]
	then
		echo "\"V (Å^3)\" \"Unit cell volume (Å^3)\"" >> $OUTFILE
	elif [ "$CHECK" == "elmod" ]
	then
		detect_epsilons $1
		echo "$EPSDIRS \"Elastic modulus (GPa)\"" >> $OUTFILE
	# for 
	elif [ "$CHECK" == "CIJ" ]
	then
		detect_epsilons $1
		echo "$EPSDIRS \"C_{${CIJTYPE}} matrix element (GPa)\"" >> $OUTFILE
	elif [ "$CHECK" == "lattice" ]
	then
		if [ "$LATTICE" == "h" ]
      then
			echo "\"a (Å)\" \"c (Å)\" \"Lattice parameter (Å)\"" >> $OUTFILE
		elif [ "$LATTICE" == "o" ]
      then
			echo "\"a (Å)\" \"b (Å)\" \"c (Å)\" \"Lattice parameter (Å)\"" >> $OUTFILE
		elif [ "$LATTICE" == "c" ]
      then
			echo "\"a (Å)\" \"Lattice parameter (Å)\"" >> $OUTFILE
		else
			echo "\"a (Å)\" \"b (Å)\" \"c (Å)\" \"Lattice parameter (Å)\"" >> $OUTFILE
		fi
	elif [ "$CHECK" == "runtime" ]
	then
      echo "\"Runtime (s)\" \"Runtime (s)\"" >> $OUTFILE
	elif [ "$CHECK" == "gap" ]
	then
      echo "\"Band gap (eV)\" \"Band gap (eV)\"" >> $OUTFILE
	elif [ "$CHECK" == "stress" ]
	then
		if [ "$CODE" == "VASP" ]
		then
			echo "\"σ_{xx} (GPa)\" \"σ_{yy} (GPa)\" \"σ_{zz} (GPa)\" \"Stress (GPa)\"" >> $OUTFILE
		elif [ "$CODE" == "qe" ]
		then
			echo "\"σ_{xx} (GPa)\" \"σ_{yy} (GPa)\" \"σ_{zz} (GPa)\" \"Stress (GPa)\"" >> $OUTFILE
		else
			#FIXME: units
			echo "\"σ_a (Hartree/Bohr^3)\" \"σ_b (Hartree/Bohr^3)\" \"σ_c (Hartree/Borh^3)\"" >> $OUTFILE
		fi
	else
		echo "make_file_header: unhandled case"
		exit 30
	fi
	debug_echo "make_file_header: done" 
}

DEBUG=0

while getopts "h?vfel:sbcC:E:Vt:gT" opt; do
   case "$opt" in
   h|\?)
      help
      exit 0
      ;;
   v) DEBUG=1
      ;;
   t) MAINTITLE="$OPTARG"
      ;;
   f) CHECKS="$CHECKS forces"
		NPROPS="$NPROPS 1"
		#FIXME:this might be broken now 
		NLINES="$NLINES 4"
		YLOGSCALE="$YLOGSCALE 1"
      ;;
   E) CHECKS="$CHECKS elmod"
		LATTICE="$OPTARG"
		YLOGSCALE="$YLOGSCALE 0"
		NPROPS="$NPROPS 1"
		NLINES="$NLINES 1"
      ;;
   e) CHECKS="$CHECKS energy"
		YLOGSCALE="$YLOGSCALE 0"
		NPROPS="$NPROPS 1"
		NLINES="$NLINES 1"
      ;;
   V) CHECKS="$CHECKS volume"
		YLOGSCALE="$YLOGSCALE 0"
		NPROPS="$NPROPS 1"
		NLINES="$NLINES 1"
      ;;
   c) CHECKS="$CHECKS CEBE"
		YLOGSCALE="$YLOGSCALE 0"
		NPROPS="$NPROPS 1"
		NLINES="$NLINES 1"
      ;;
   T) CHECKS="$CHECKS runtime"
		YLOGSCALE="$YLOGSCALE 0"
		NPROPS="$NPROPS 1"
		NLINES="$NLINES 1"
      ;;
   g) CHECKS="$CHECKS gap"
		YLOGSCALE="$YLOGSCALE 0"
		NPROPS="$NPROPS 1"
		NLINES="$NLINES 1"
      ;;
   C) CHECKS="$CHECKS CIJ"
		YLOGSCALE="$YLOGSCALE 0"
		#FIXME: allow multiple
      CIJTYPES="$CIJTYPES $OPTARG"
		CIJIND=-1
		NPROPS="$NPROPS 1"
		NLINES="$NLINES 1"
      ;;
   b) CHECKS="$CHECKS bulk_modulus"
		NPROPS="$NPROPS 1"
		YLOGSCALE="$YLOGSCALE 0"
		NLINES="$NLINES 1"
      ;;
   l) CHECKS="$CHECKS lattice"
		LATTICE="$OPTARG"
		debug_echo "detected lattice type $LATTICE"
		if [ "$LATTICE" == "h" ]
		then
			NPROPS="$NPROPS 2"
		   YLOGSCALE="$YLOGSCALE 0 0"
		elif [ "$LATTICE" == "c" ]
		then
			NPROPS="$NPROPS 1"
		   YLOGSCALE="$YLOGSCALE 0"
		elif [ "$LATTICE" == "t" ]
		then
			NPROPS="$NPROPS 2"
		   YLOGSCALE="$YLOGSCALE 0 0"
		else
			NPROPS="$NPROPS 3"
		   YLOGSCALE="$YLOGSCALE 0 0 0"
		fi
		NLINES="$NLINES 1"
      ;;
   s) CHECKS="$CHECKS stress"
		NPROPS="$NPROPS 3"
		YLOGSCALE="$YLOGSCALE 0 0 0"
		NLINES="$NLINES 1"
      ;;
   esac
done
shift $((OPTIND-1))

#create arrays from some strings to make access easier
NPROPS=($NPROPS)
NLINES=($NLINES)
YLOGSCALE=($YLOGSCALE)
CIJTYPES=($CIJTYPES)

#if (($# < 1))
#then
#   echo "Error: No arguments given"
#   help
#   exit 0
#fi

if [ "$CHECKS" == "" ]
then
	echo "Error: no check type specified"
	help
	exit 0
else
	debug_echo "detected checks: $CHECKS"
fi

mkdir conv_results

find_types
detect_code
#make plot header needs the total amount of plots
NPROP=0
for i in ${NPROPS[@]}
do
	NPROP=$((NPROP+i))
done
make_plot_header
add_settings

for t in $TYPES
do
	findbest $t
	NUM=0
	debug_echo "setting CIJIND to 0"
	CIJIND=0
	for CHECK in $CHECKS
	do
		NPROP=${NPROPS[$NUM]}
		NLINE=${NLINES[$NUM]}
		if [ "$CHECK" == "CIJ" ]
		then
			CIJTYPE=${CIJTYPES[$CIJIND]}
			SUF="$CIJTYPE"
		else
			SUF=""
		fi
		OUTFILE="conv_results/${t}-${CHECK}$SUF.txt"
		rm -f "$OUTFILE"
		make_file_header $t
		for i in ${t}_*
		do
			val=$(getvalue "$i")
			res=$($CHECK "$i")
			echo "$val $res" >> "$OUTFILE"
		done
		add_subplot $t
		NUM=$((NUM + 1))

		if [ "$CHECK" == "CIJ" ]
      then
			CIJIND=$((CIJIND + 1))
			debug_echo "setting CIJIND to $CIJIND"
		fi
	done
done

gnuplot conv_results/plot.gpi > conv_results/results.pdf
	
exit 0

# vim: set tabstop=3:softtabstop=3:shiftwidth=2:
