#!/usr/bin/env python3

from sys import argv
import argparse
from common import read_structure

parser = argparse.ArgumentParser(description='Calculate the mass density from a Wien2k, VASP or OpenMX file')
parser.add_argument("input_file", help='path to the structure file')
args = parser.parse_args()

s = read_structure(args.input_file)

V = s.get_volume() * 1e-24
w = sum(s.get_masses())

print('{0:.8}'.format(w * 1.66053904e-24 / V))
