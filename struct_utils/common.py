import numpy as np
import re
import os
from ase import Atoms, Atom, io

# ------ general stuff ----

def atrad(x):
    return {
      'B': 0.85,
      'C': 0.71,      
      'N': 0.65,
      'O': 0.60,
      'Si': 1.1,
      'Al': 1.25,
      'Ti': 1.40,
      'Mo': 1.45,
      'Ta': 1.45,
      'W': 1.35,
    }[x]

def read_structure(fl):
    '''detects the input file and calls the correct read function'''
    p = re.compile(r'.*\.dat#*$')

    if fl.endswith('.struct'):
        return io.read(fl, format="struct")
    if fl.endswith('.cif'):
        return io.read(fl, format="cif")
    elif fl.endswith('.in'):
        return io.read(fl, format="espresso-in")
    elif fl.endswith('CAR') or fl.endswith('.vasp') or fl.endswith('car'):
        return io.read(fl, format="vasp")
    elif p.match(fl):
        return readopenmx(fl)
    else:
        return io.read(fl)

# ------- QuantumESPRESSO specific stuff ------

def qe_format_value(value):
    p = re.compile(r'^-*[0-9]+\.*[0-9]*[eEdD]*-*[0-9]*$') 
    if value.isdigit():
        return int(value)
    elif value.lower() == '.false.':
        return False
    elif value.lower() == '.true.':
        return True
    elif p.match(value):
        return float(value.replace('d','e').replace('D','e'))
    else:
        return value

def read_qe_settings(inp):
    f = open(inp, "r")
    read = 0
    species = 0
    kpoints = 0
    pps = {}
    settings = {}

    for line in f:
        line = line.strip()

        if line == "":
            continue

        if line[0] == "&":
            read = 1
            continue

        if line == "ATOMIC_SPECIES":
            species = 1
            continue

        if "ATOMIC_POSITIONS" in line or "CELL_PARAMETERS" in line:
            species = 0
            continue

        if "K_POINTS" in line:
            species = 0
            l = line.lower()
            if "gamma" in l:
                kpts=None
                koffset=[0, 0, 0]
                continue
            if "automatic" in l:
                kpoints = 1
            else:
                print("Unsupported k-point settings")
            continue

        if line == "/":
            read = 0
            continue

        if species == 1:
            l = line.split()
            pps[l[0]] = l[2] 

        if kpoints == 1:
            l = line.split()
            kpts = (int(l[0]), int(l[1]), int(l[2]))
            koffset = (int(l[3]), int(l[4]), int(l[5]))
            kpoints = 0
            continue

        if read == 1:
            l = line.split('=')
            if len(l) != 2:
                print("Unexpected length")
            name = l[0].strip()
            value = l[1].strip().strip("'").strip("\"")
            if name == "ibrav" or name == "nat" or name == "ntyp":
                continue

            finalvalue=qe_format_value(value)

            # handle the pseudopotentials path properly
            if name == "pseudo_dir":
                if not os.path.isabs(finalvalue):
                    path = os.path.dirname(inp) + '/' + finalvalue
                    abspath = os.path.abspath(path)
                    finalvalue = abspath
            
            settings[name] = finalvalue

    return settings, pps, kpts, koffset

# ------- WIEN2k specific stuff ------

def R0(x):
    if x < 19:
        r = 1.0e-4
    elif x < 37:
        r = 5.0e-5
    elif x < 72:
        r = 1.0e-5
    else:
        r = 5.0e-6
    return r

def printWIEN2k(s,atnumbers):
    A2B = 1.8897259886
    cell = s.cell.cellpar()

    print("generated struct file")
    print("P   LATTICE,NONEQUIV.ATOMS%4.i    1 P1    " % len(s))
    print("MODE OF CALC=RELA unit=bohr")
    print("%10.6f%10.6f%10.6f%10.6f%10.6f%10.6f" % (cell[0] * A2B, cell[1] * A2B, cell[2] * A2B, cell[3], cell[4], cell[5]))
    for i,at in enumerate(s):
        p = at.scaled_position
        r = "%.9f" % R0(at.number)
        print("ATOM%4i: X=%10.8f Y=%10.8f Z=%10.8f" % (-(i + 1), p[0] % 1.0, p[1] % 1.0, p[2] % 1.0))
        print("          MULT= 1          ISPLIT=15")
        if atnumbers:
            print("%-2s%-3i      NPT=  781  R0=%s RMT=   2.00000   Z:%10.5f" % (at.symbol, i+1, r[1:], at.number))
        else:
            print("%-2s         NPT=  781  R0=%s RMT=   2.00000   Z:%10.5f" % (at.symbol, r[1:], at.number))
        print("LOCAL ROT MATRIX:    1.0000000 0.0000000 0.0000000")
        print("                     0.0000000 1.0000000 0.0000000")
        print("                     0.0000000 0.0000000 1.0000000")
    print("   0      NUMBER OF SYMMETRY OPERATIONS")

# ----- openmx stuff -----

def spincharge(x):
    return {
      'H': [0.5, 0.5],
      'B': [1.5, 1.5],
      'C': [2.0, 2.0],
      'O': [3.0, 3.0],
      'Ti': [6.0, 6.0],
      'V': [6.5, 6.5],
      'N': [2.5, 2.5],
      'Al': [1.5, 1.5],
      'Si': [2.0, 2.0],
      'Mo': [7.0, 7.0],
      'W': [6.0, 6.0],
      'Ta': [6.5, 6.5],
    }[x]

def atspecdef(x):
    return {
      'H': ['H6.0-s2p1','H_PBE19'],
      'B': ['B7.0-s2p2d1','B_PBE19'],
      'C': ['C6.0-s2p2d1','C_PBE19'],
      'O': ['O6.0-s2p2d1','O_PBE19'],
      'Si': ['Si7.0-s2p2d1','Si_PBE19'],
      'Ti': ['Ti7.0-s3p2d1','Ti_PBE19'],
      'V': ['V6.0-s3p2d1','V_PBE19'],
      'N': ['N6.0-s2p2d1','N_PBE19'],
      'Al': ['Al7.0-s2p2d1','Al_PBE19'],
      'W': ['W7.0-s3p2d2f1','W_PBE19'],
      'Mo': ['Mo7.0-s3p2d2','Mo_PBE19'],
      'Ta': ['Ta7.0-s3p2d2f1','Ta_PBE19'],
    }[x]

def printopenmx(s):
    atypes=set([x.symbol for x in s])

    print("Species.Number %i" % len(atypes))
    print("<Definition.of.Atomic.Species")
    for at in sorted(atypes):
        b = atspecdef(at)
        print("{0:>4} {1:>20} {2:>10}".format(at, b[0], b[1]))
    print("Definition.of.Atomic.Species>\n")

    print("Atoms.Number %i" % len(s) )
    print("Atoms.SpeciesAndCoordinates.Unit FRAC")
    print("<Atoms.SpeciesAndCoordinates")

    for i,at in enumerate(s):
        p = at.scaled_position
        print("%2i %4s %12.8f %12.8f %12.8f %4.1f %4.1f" % (i+1, at.symbol, p[0], p[1], p[2], spincharge(at.symbol)[0], spincharge(at.symbol)[1]))

    print("Atoms.SpeciesAndCoordinates>\n")

    print("Atoms.UnitVectors.Unit Ang")
    print("<Atoms.UnitVectors")
    cell = s.get_cell()
    print("%12.8f %12.8f %12.8f" % (cell[0,0], cell[0,1], cell[0,2]))
    print("%12.8f %12.8f %12.8f" % (cell[1,0], cell[1,1], cell[1,2]))
    print("%12.8f %12.8f %12.8f" % (cell[2,0], cell[2,1], cell[2,2]))
    print("Atoms.UnitVectors>")

def readopenmx(fl):
    a = Atoms(pbc=[True, True, True])
    exp_cell= 0
    exp_at = 0
    scale = 1.0
    frac = -1

    with open(fl) as f:
        for line in f:
            line = ' '.join(line.split())
            line = line.split('#')[0] #get rid of the comments
            if len(line) < 1:
                continue

            if exp_cell == 1:
                a.cell[0][:] = np.array([float(x) * scale for x in line.split(" ")])
                exp_cell += 1
                continue

            if exp_cell == 2:
                a.cell[1][:] = np.array([float(x) * scale for x in line.split(" ")])
                exp_cell += 1
                continue
            if exp_cell == 3:
                a.cell[2][:] = np.array([float(x) * scale for x in line.split(" ")])
                exp_cell += 1
                continue

            if "Atoms.UnitVectors.Unit".lower() in line.lower():
                if "au" in line.lower():
                    scale = 0.529177249

            if "<Atoms.UnitVectors" in line:
                exp_cell = 1
                continue

            if "Atoms.SpeciesAndCoordinates.Unit".lower() in line.lower():
                if "FRAC".lower() in line.lower():
                    frac = 1
                    atmp = []
                elif "Ang".lower() in line.lower():
                    frac = 0
                else:
                    print("Unrecognized units in:", line)

            if "<Atoms.SpeciesAndCoordinates" in line:
                exp_at=1
                continue

            if "Atoms.SpeciesAndCoordinates>" in line:
                exp_at=0
                continue

            if exp_at == 1:
                tmp = line.split(" ")
                # skip empty atoms
                if tmp[1][0] == 'E' and tmp[1][1] != 'r' and tmp[1][1] != 's':
                    continue
                if frac == 1:
                    x = (float(tmp[2]) + 1) % 1
                    y = (float(tmp[3]) + 1) % 1
                    z = (float(tmp[4]) + 1) % 1
                    a.append(Atom(tmp[1], (0,0,0)))
                    atmp.append([x, y, z])
                elif frac == 0:
                    x = float(tmp[2])
                    y = float(tmp[3]) 
                    z = float(tmp[4])
                    a.append(Atom(tmp[1], (x,y,z)))
                else:
                    print("ERROR")
    if frac == 1:
        a.set_scaled_positions(atmp)
    return a

# vim: set expandtab tabstop=4 shiftwidth=4:
