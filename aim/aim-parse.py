#!/usr/bin/env python2

from sys import argv
from math import *
from numpy import std,arccos
import argparse
import re
import sys
from read_struct import read_struct_file,dist

def fmaxg(a1, a2, c):
	"doscstring"
	if a1 + a2 in c:
		return c[a1 + a2]
	elif a1 in c:
		return c[a1]
	else:
		return c['other']

def parse_cutoff_file(f):
	c = {}
	for line in open(f):
		el = line.split()
		if len(el) == 3:
			c[el[0] + el[1]] = float(el[2])
			c[el[1] + el[0]] = float(el[2])
		elif len(el) == 2:
			cutoffs[el[0]] = float(el[1])
		elif len(el) == 1:
			c['other'] = float(el[0])
	return c

def angle(atom1, atom2, atom3, csize):
	"function_docstring"
	sum1 = 0.0
	sum2 = 0.0
	sc = 0.0
	for x in range(3):
		dif1 = atom2[x+1] - atom1[x+1]
		dif2 = atom2[x+1] - atom3[x+1]
		if abs(dif1) > csize[x]/2.0:
			sum1 += (csize[x] - abs(dif1)) * (csize[x] - abs(dif1))
			if atom2[x+1] > atom1[x+1]:
				dif1 = -(csize[x] - abs(dif1))
			else:
				dif1 = (csize[x] - abs(dif1))
		else:
			sum1 += dif1 * dif1
		if abs(dif2) > csize[x]/2.0:
			sum2 += (csize[x] - abs(dif2)) * (csize[x] - abs(dif2))
			if atom2[x+1] > atom3[x+1]:
				dif2 = -(csize[x] - abs(dif2))
			else:
				dif2 = (csize[x] - abs(dif2))
		else:
			sum2 += dif2 * dif2
		sc += dif1 * dif2
	return arccos(sc / sqrt(sum1) / sqrt(sum2)) / 3.14 * 180


#	vec1 = [atom2[i+1] - atom1[i+1] for i in range(3)]
#	vec2 = [atom3[i+1] - atom2[i+1] for i in range(3)]
#	sc = vec1[0] * vec2[0] + vec1[1] * vec2[1] + vec1[2] * vec2[2]
#	absv1 = sqrt(vec1[0] ** 2 + vec1[1] ** 2 + vec1[2] ** 2)
#	absv2 = sqrt(vec2[0] ** 2 + vec2[1] ** 2 + vec2[2] ** 2)
#	return arccos(sc / (absv1 * absv2)) / 3.14 * 180

def mean(data):
	"""Return the sample arithmetic mean of data."""
	n = len(data)
	if n < 1:
		return 0.0
	return float(sum(data))/float(n) # in Python 2 use sum(data)/float(n)

def wmean(data):
	"""Return the weighted sample arithmetic mean of data."""
	#print data
	if len(data) < 1:
		return "-"
	wsum = 0.0
	n = 0.0
	for x in data:
		wsum += x[0] * x[2]
		n += x[2]

	return wsum / n

def wstdev(data):
	"""Return the weighted stdev of data."""
	#print data
	if len(data) < 1:
		return "-"
	x0 = wmean(data)
	wsum = 0.0
	n = 0.0
	for x in data:
		wsum += (x[0] - x0)**2 * x[2]
		n += x[2]

	return (wsum * len(data) / n / (len(data) - 1) ) ** 0.5
#	return (wsum / n / len(data) ) ** 0.5

def wuncavg(data):
	 """Return the uncertainty of the weighted average."""
	 #print data
	 if len(data) < 1:
		  return "-"
	 x0 = wmean(data)
	 wsum = 0.0
	 n = 0.0
	 for x in data:
		  wsum += (x[0] - x0)**2 * x[2]
		  n += x[2]

	 return (wsum / n / len(data) ) ** 0.5


def _ss(data):
	"""Return sum of square deviations of sequence data."""
	c = mean(data)
	ss = sum((x-c)**2 for x in data)
	return ss

def pstdev(data):
	"""Calculates the population standard deviation."""
	n = len(data)
	if n < 2:
		return 0.0
	ss = _ss(data)
	pvar = ss/(n-1) # the population variance	
	return pvar**0.5

def puncert(data):
	 n = len(data)
	 if n < 2:
		  return 0.0
	 return pstdev(data)/sqrt(len(data))

def print_stats(cn,nn,suf):
	"""Prints statistics about atomic environments and average peak positions"""
	neighbor_types = cn[1]
	aname = cn[0]
	if suf != "":
		suf = "-" + suf
	f = open(args.p + aname + "-bader-stats" + suf + ".txt", "w")

	# Give some stats about the overall levels without any division
	f.write("------- Overall charges -------\n\n")
	data=[i[2] for i in bind_enes[nat]]

	f.write("average charge: " + str(mean(data)) + "\n")
	f.write("charge stdev: " + str(pstdev(data)) + "\n")
	f.write("charge uncertainty " + str(puncert(data)) + "\n")
	f.write("list of charges: " + "\n")
	f.write("\n".join(str(x) for x in data) + "\n\n")

	f.write("------- All neighbor types -------\n\n")

	for ntype in neighbor_types:
		enes = []
		for l in bind_enes[nat]:
			if cmp(ntype,neighbors[l[0] - 1][1]) == 0:
				enes.append(float(l[2]))
		f.write(ntype + "\n")
		f.write("number of atoms in this configuration: " + str(len(enes)) + "\n")
		f.write("average position: " + str(mean(enes)) + "\n")
		f.write("position stdev: " + str(pstdev(enes)) + "\n")
		f.write("position uncertainty " + str(puncert(enes)) + "\n")
		f.write("list of positions: " + "\n")
		f.write("\n".join(str(x) for x in enes) + "\n\n")

	f.write("------- Simplified types based on number of neighbors -------\n\n")

	for n in nn[1]:
		enes = []
		for l in bind_enes[nat]:
			if neighbors[l[0] - 1][3] == n:
				enes.append(float(l[2]))
		f.write(str(n) + "-coordinated" + "\n")
		f.write("number of atoms in this configuration: " + str(len(enes)) + "\n")
		f.write("average position: " + str(mean(enes)) + "\n")
		f.write("position stdev: " + str(pstdev(enes)) + "\n")
		f.write("position uncertainty " + str(puncert(enes)) + "\n")
		f.write("list of positions: " + "\n")
		f.write("\n".join(str(x) for x in enes) + "\n\n")

parser = argparse.ArgumentParser(description='Solve bader charge data into subpeaks')
group = parser.add_mutually_exclusive_group(required=True)
group.add_argument('-aim', help='path to the outputaim Wien2k file', type=str)
group.add_argument('-geom', help='path to the xyz struct file', action='store_const', const=1)
group2 = parser.add_mutually_exclusive_group(required=False)
group2.add_argument('-cf', help='path to file with bond cutoff distances', dest='cutoff_file')
group2.add_argument('-c', help='bond cutoff length (default 2.4 Angstrom)', type=float, default=2.4)
parser.add_argument('-p', help='prefix for output files', type=str, default="")
parser.add_argument('-suf', help='suffix for output files', type=str, default="")
parser.add_argument('-nospec', help='do not output spectra', action='store_const', const=0, default=1)
parser.add_argument('-coord', help='output atomic coordination numbers', action='store_const', const=1)
parser.add_argument("struct_file")
parser.add_argument("bader_charge_file")
args = parser.parse_args()

struct_info = read_struct_file(args.struct_file)
data = struct_info[0]
csize = struct_info[1]
atomtypes = struct_info[2]

neighbors = []
for i,atom1 in enumerate(data):
	neighbors.append([])
	neighbors[i].append(atom1[0])
	neighbors[i].append([])
	neighbors[i].append([])

cutoffs = {}
if args.cutoff_file:
	cutoffs = parse_cutoff_file(args.cutoff_file)
else:
	cutoffs['other'] = args.c

# calculate nearest neighbors manually
if args.geom:
	for i,atom1 in enumerate(data):
		for j,atom2 in enumerate(data):
			if i == j:
				continue
			if dist(atom1, atom2, csize) < fmaxg(atom1[0],atom2[0], cutoffs):
				neighbors[i][1].append(atom2[0])
				neighbors[i][2].append(j)
		sorted(neighbors[i][1])
		sorted(neighbors[i][2])
		neighbors[i].append(len(neighbors[i][1]))
		neighbors[i][1] = "".join(neighbors[i][1])

# get nearest neighbors from aim output file 
if args.aim:
	for line in open(args.aim):
		p = line.split()
		if len(p) == 14 and p[0] == ':PC' and p[7] == '-1':
			at1 = -int(p[10])-1
			at2 = -int(p[12])-1
			#print at1,at2
			neighbors[at1][1].append(at2)
			neighbors[at2][1].append(at1)
	for n in neighbors:
		print n[1]
		a = set(n[1])
		n[1] = [] 
		for at in a:
			n[1].append(data[at][0])

# output coordination numbers
if args.coord:
	 for a in atomtypes:
		  c = []
		  d = {}
		  for i,n in enumerate(neighbors):
				if n[0] == a:
					 c.append(n[3])
					 if d.has_key(n[3]):
						  d[n[3]].append(i - 63) ## FIXME: this as arbitrary
					 else:
						 d[n[3]] = [i - 63]
		  c.sort()
		  print a, mean(c), std(c)#, c[int(round(len(c) * 0.25))] - mean(c), c[int(round(len(c) * 0.75))] - mean(c)
		  print d

# find all unique neighboir configuration for each atom	
cnumtypes = []
numneigh = []
for i,atomtype in enumerate(atomtypes):
	cnumtypes.append([atomtype])
	numneigh.append([atomtype])
	cnumtypes[i].append([])
	numneigh[i].append([])
	for atom in neighbors:
		if atom[0] == atomtype:
			cnumtypes[i][1].append(atom[1])	
			numneigh[i][1].append(atom[3])
	cnumtypes[i][1] = set(cnumtypes[i][1])
	numneigh[i][1] = set(numneigh[i][1])

# open aim data
bind_enes = [[] for a in atomtypes]
for line in open(args.bader_charge_file):
	line = line.split()
	i = atomtypes.index(line[1])
	energy = float(line[2])
	bind_enes[i].append([int(line[0]), 1, energy])
	
# iterate over list of unique atoms
for nat,type in enumerate(cnumtypes):
	#print neighbors
	print_stats(cnumtypes[nat], numneigh[nat], args.suf)

# vim: tabstop=3 shiftwidth=3 noexpandtab
