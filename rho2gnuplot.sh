#!/bin/bash
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright (c) 2019 Pavel Ondračka
#
# This is a simple script to convert the rho 2D charge density format
# as calculated by WIEN2k-lapw5 to the gnuplot matrix image format

awk 'BEGIN {t = 1}
     {
        if (NR == 1) {
           na = $1;
           nb = $2;
           a = $3;
           b = $4;
           print "#", $1, $2, $3, $4
        } else {
           for (i = 1; i <= NF; i++) {
              if(t < nb) {printf "%.3e ", $i; t++}
              else {printf "%.3e\n", $i; t = 1}
           }
        }
     }' $1
