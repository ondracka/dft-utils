#!/usr/bin/env python3

from pymatgen.io.vasp import Poscar, Outcar
from os.path import isfile, isdir, join, abspath, exists
from os import listdir
from numpy.linalg import inv, pinv
import os
import re
import glob
import numpy as np
import sys
import argparse
from ase import io
from elastic import *

import json

parser = argparse.ArgumentParser(description='Calculate elastic tensor using the stress-strain method')
parser.add_argument('-q', help='use QUANTUM Espresso', action='store_true')
parser.add_argument('-N', help='output NOMAD metadata', action='store_true')
args = parser.parse_args()

EQUILIBRIUM_DIRECTORY = 'eq'
WORKING_DIRECTORY_PREFIX = 'eps='
CASE_PREFIX = 'eps'
CASES = 6
PRINT_WITH_SYMPY = False

def output_NOMAD(Cij, averages, struct, eigenval):
    data = {}
    data['metadata'] = {}
    data['metadata']['domain'] = 'dft'
    data['run'] = [{}]

    program = {}
    program['name'] = "custom elastic calculator"
    program['version'] = "0.1"
    data['run'][0]['program'] = program

    system = [{}]
    data['run'][0]['system'] = system

    atoms = {}
    atoms['species'] = struct.get_atomic_numbers().tolist()
    atoms['labels'] = struct.get_chemical_symbols()
    atoms['positions'] = (struct.get_positions() * 1e-10).tolist()
    atoms['lattice_vectors'] = (struct.get_cell() * 1e-10).tolist()
    atoms['periodic'] = [ True, True, True ]

    system[0]['atoms'] = atoms
    
#   material = {}
#   material['chemical_formula_hill'] = struct.get_chemical_formula()
#   material['chemical_formula_descriptive'] = struct.get_chemical_formula()
#   material['elements'] = list(dict.fromkeys(struct.get_chemical_symbols()))
    
    data['workflow2'] = {}
    data['workflow2']['m_def'] = "nomad.datamodel.metainfo.simulation.workflow.Elastic"
    elastic = {}
    data['workflow2']['results'] = elastic
    method = {}
    if args.q:
        method['energy_stress_calculator'] = "Quantum ESPRESSO"
    else:
        method['energy_stress_calculator'] = "VASP"
    method['calculation_method'] = "stress"
    data['workflow2']['method'] = method
    method['elastic_constants_order'] = 2
    elastic['elastic_constants_matrix_second_order'] = (Cij * 1e9).tolist()
    elastic['bulk_modulus_voigt'] = averages[0][0] * 1e9
    elastic['young_modulus_voigt'] = averages[0][1] * 1e9
    elastic['shear_modulus_voigt'] = averages[0][2] * 1e9
    elastic['poisson_ratio_voigt'] = averages[0][3]
    elastic['bulk_modulus_reuss'] = averages[1][0] * 1e9
    elastic['young_modulus_reuss'] = averages[1][1] * 1e9
    elastic['shear_modulus_reuss'] = averages[1][2] * 1e9
    elastic['poisson_ratio_reuss'] = averages[1][3]
    elastic['bulk_modulus_hill'] = averages[2][0] * 1e9
    elastic['young_modulus_hill'] = averages[2][1] * 1e9
    elastic['shear_modulus_hill'] = averages[2][2] * 1e9
    elastic['poisson_ratio_hill'] = averages[2][3]
    elastic['eigenvalues_elastic'] = [i * 1e9 for i in eigenval]
    if eigenval[0] < 0.0:
        elastic['is_mechanically_stable'] = False
    else:
        elastic['is_mechanically_stable'] = True

    f = open('elastic.archive.json', 'w+')
    json.dump(data, f, indent=2)

def reverse_readline(filename, buf_size=8192):
    """a generator that returns the lines of a file in reverse order"""
    with open(filename) as fh:
        segment = None
        offset = 0
        fh.seek(0, os.SEEK_END)
        file_size = remaining_size = fh.tell()
        while remaining_size > 0:
            offset = min(file_size, offset + buf_size)
            fh.seek(file_size - offset)
            buffer = fh.read(min(remaining_size, buf_size))
            remaining_size -= buf_size
            lines = buffer.split('\n')
            if segment is not None:
                if buffer[-1] is not '\n':
                    lines[-1] += segment
                else:
                    yield segment
            segment = lines[0]
            for index in range(len(lines) - 1, 0, -1):
                if len(lines[index]):
                    yield lines[index]
        # Don't yield None if the file was empty
        if segment is not None:
            yield segment

def stress_from_outcar(outcar):
    stress_pattern = re.compile(r'in kB(\s+([\d\-\.]+)){6}')

    for line in reverse_readline(outcar):
        pat = stress_pattern.search(line)
        if pat:
            extracted_line = pat.group(0)
            stress_raw =  np.array([float(s)/10 for s in  \
                    re.findall('[\d\-\.]+', extracted_line)])
            #Reorder to comply to Voigt notation
            stress = np.zeros((6,))
            stress[0] = stress_raw[0]
            stress[1] = stress_raw[1]
            stress[2] = stress_raw[2]
            stress[3] = stress_raw[4]
            stress[4] = stress_raw[5]
            stress[5] = stress_raw[3]
            return stress

if __name__ == '__main__':
    np.set_printoptions(precision=7, suppress=True, formatter={'float': '{: 0.6f}'.format})
    working_dir = os.getcwd()
    strains = []
    stresses = []
    print('Current working directory: {0}'.format(working_dir))

    try:
        calculation_directories = list(
            filter(
                lambda pth: isdir(abspath(pth)) and \
                    pth.startswith(WORKING_DIRECTORY_PREFIX),
                listdir(working_dir)
            )
        )
    except:
        print('Could not find calculation directory')
        sys.exit()

    if args.q:
        print(working_dir + '/' +  EQUILIBRIUM_DIRECTORY + "/*.out")
        outfile = glob.glob(EQUILIBRIUM_DIRECTORY + "/*.out")
        if len(outfile) != 1:
            print("Found {} equilibrium output files".format(len(outfile)))
            sys.exit()
        eq_matrix = np.array(io.read(outfile[0], format="espresso-out").cell)
        print(eq_matrix)
        
    else:
        eq_structure_path = join(working_dir, EQUILIBRIUM_DIRECTORY, 'CONTCAR')

        if exists(eq_structure_path):
            try:
                eq_matrix = Poscar.from_file(eq_structure_path).structure.lattice.matrix
            except:
                print('Could not load equilibrium structure')
                sys.exit()
        else:
            print('Could not find equilibrium structure file. Expected existing file {0}'.format(eq_structure_path))
    for calculation_directory in calculation_directories:
        print('Calculation directory: {0}'.format(calculation_directory))
        calculation_directory = join(working_dir, calculation_directory)

        for case in [j for k in [(i,-i) for i in range(1,CASES+1)] for j in k]:
            case_dir = '{0}{1}'.format(CASE_PREFIX, case)
            case_dir_path = join(working_dir, calculation_directory, case_dir)
            if not exists(case_dir_path):
                print('Could not find directory: {0}'.format(case_dir))
                sys.exit()

            if args.q:
                outfile = glob.glob(case_dir_path + "/*.out")
                if len(outfile) != 1:
                    print("Found {} output files in {}".format(len(outfile), case_dir_path))
                    sys.exit()
                struct = io.read(outfile[0], format="espresso-out")
                case_lattice_mat = np.array(struct.cell)
                stress_vec = struct.calc.get_stress() * -160.21766208
                #print(stress_vec)

            else:
                contcar_path = join(case_dir_path, 'CONTCAR')
                outcar_path = join(case_dir_path, 'OUTCAR')
                if not exists(contcar_path):
                    print('Could not find CONTCAR in directory {0}'.format(case_dir_path))
                    sys.exit()
                if not exists(outcar_path):
                    print('Could not find OUTCAR in directory {0}'.format(case_dir_path))
                    sys.exit()

                struct = io.read(eq_structure_path, format="vasp")
                case_lattice_mat = Poscar.from_file(contcar_path).structure.lattice.matrix
                stress_vec = stress_from_outcar(outcar_path)

            deformation_mat = np.matmul(inv(eq_matrix), case_lattice_mat) - np.identity(3)
            deformation_vec = np.array([deformation_mat[0,0],
                                        deformation_mat[1,1],
                                        deformation_mat[2,2],
                                        2*deformation_mat[1,2],
                                        2*deformation_mat[0,2],
                                        2*deformation_mat[0,1]])
            strains.append(deformation_vec)
            stresses.append(stress_vec)
        epsilon = np.array(strains)
        sigma = np.array(stresses)
        if epsilon.shape == sigma.shape:
            Cij = np.matmul(pinv(epsilon), sigma)
            Cij = -0.5 * (Cij + Cij.T)
            np.savetxt(join(calculation_directory,'Cij.dat'), Cij, fmt='%.7f')

            if PRINT_WITH_SYMPY:
                from sympy import Matrix, pprint
                Cij_mat = Matrix(Cij)
                pprint(Cij_mat)
            else:
                print(Cij)

            el = Elastic(str(Cij).replace(' [', '').replace('[', '').replace(']', ''))
            averages = el.averages()

            eigenval = sorted(np.linalg.eig(el.CVoigt)[0])
            if eigenval[0] < 0.0:
                print("Warning: Is not mechanically stable!")
            print("Hills K: " + str(averages[2][0]))
            print("Hills E: " + str(averages[2][1]))
            print("Hills G: " + str(averages[2][2]))
            print("Hills gamma: " + str(averages[2][3]))

            if args.N:
                output_NOMAD(Cij, averages, struct, eigenval)

# vim: set expandtab tabstop=4 shiftwidth=4:
