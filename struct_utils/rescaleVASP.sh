#!/bin/bash

awk '{if (NR == 2) {a=$1; printf "%.5f\n", 1.0;} else if (NR>1 && NR < 6) printf "%.10f %.10f %.10f\n", $1*a, $2*a, $3*a; else print }' $1
