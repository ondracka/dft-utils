#!/bin/bash

rm D.txt D44.txt
for D in D44_*
do
   A=$(sed s/D44_//g <<< "$D")
   ENE1=$(cat D_$A/OUTCAR | grep "energy without" | tail -n1 | awk '{print $8}')
   ENE2=$(cat D44_$A/OUTCAR | grep "energy without" | tail -n1 | awk '{print $8}')
   echo "$A $ENE1" >> D.txt
   echo "$A $ENE1" >> D44.txt
   if [ "$A" == "0" ]
   then
      cp $MYWORKDIR/DFT/EOS-fit/D-EOS.gpl .
      sed -i "s/E_0 = .*/E_0 = $ENE1/g" D-EOS.gpl
      VOL=$(cat D_$A/OUTCAR | grep "volume" | tail -n1 | awk '{print $5}')
      sed -i "s/V_0 = .*/V_0 = $VOL/g" D-EOS.gpl
   fi
done
TMP=$(gnuplot D-EOS.gpl 2>&1 >/dev/null)
echo "$TMP"
#grep -A4 "Final set of parameters" <<< "$TMP" | tail -n1 | awk '{print $3,$5}'

