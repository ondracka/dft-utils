#!/usr/bin/env python3

from sys import stdout, exit
import argparse
from common import read_qe_settings, qe_format_value
from ase import io

parser = argparse.ArgumentParser(description='Merge the QE settings from one input file and structure from other one or alternativelly from output file.')
parser.add_argument('-c', help='output crystal coordinates', action='store_true', default=False)
parser.add_argument('-k', help='specify k-point grid, example "2 5 6" or "gamma"', type=str)
parser.add_argument('-p', help='specify optional parameter(s), example "spin=2 startingpot=file"', type=str)
parser.add_argument("input_file", help='path to the .in file with settings')
parser.add_argument("struct_file", help='path to the file with structure')
args = parser.parse_args()

try:
    struct = io.read(args.struct_file, format='espresso-in')
except:
    try:
        struct = io.read(args.struct_file, format='espresso-out')
    except:
        try:
            struct = io.read(args.struct_file)
        except:
            print("Failed to read the " + args.struct_file + " structure file.")
            exit()
params, pps, kpts, koffset = read_qe_settings(args.input_file)

if args.k:
    if "gamma" in args.k:
        kpts=None
    else:
        kpts = [int(i) for i in args.k.split()]

if args.p:
    for p in args.p.split():
        l = p.split('=')
        params[l[0]] = qe_format_value(l[1])

io.write(stdout, struct, format='espresso-in', input_data=params, pseudopotentials=pps, kpts=kpts, koffset=koffset, crystal_coordinates=args.c)

# vim: set expandtab tabstop=4 shiftwidth=4:
