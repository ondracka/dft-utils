#!/bin/bash
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright (c) 2019 Pavel Ondračka
#
# This is a simple script to calculate the difference between two charge
# densities in the rho 2D charge density format as calculated by WIEN2k-lapw5
#
# Usage: rhodidd <rho1> <rho2>

paste $1 $2 | \
awk 'BEGIN {t = 1}
     {
        if (NR == 1) {
           if ($1 != $5 || $2 != $6 || $3 != $7 || $4 != $8) {
              print "Files have different number of points";
              exit;
           }
           print $1, $2, $3, $4
        } else {
           for (i = 1; i <= NF / 2; i++) {
              j = i + NF / 2;
              if(t < NF / 2) {printf "%.3e ", $i-$j; t++}
              else {printf "%.3e\n", $i - $j; t = 1}
           }
        }
     }'
