#!/bin/bash

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright (c) 2016-2020 Pavel Ondračka.

#TODO: check early that we have a jobscript if using a scheduler

function help {
   echo "Script to calculate core electron binding energies using Wien2k"
   echo ""
   echo "Usage: XPS.sh [option] [atoms:levels] [atoms:levels] ..."
   echo "atoms are selected by their number or name from struct file"
   echo "multiple atoms can be selected with ',' as separator and"
   echo "'-' to mark ranges, eg. 1,2,5 or 5-10,12-17 or O,Ti ."
   echo "The same syntax goes for core levels (names not accepted)."
   echo ""
   echo "-h print this help and exit"
   echo "-p print list of atoms with core levels"
   echo "-v output more information"
   echo "-d dry run (don't start any calculations)"
   echo "-m [method] slater, dscf or forb (default slater)"
   echo "-c [charge placement] where should the extra charge be placed:"
   echo "   conduction band (cond) or background (bg), (default bg)"
   echo "-f [filename] a job submission script (standard run_lapw otherwise) "
   echo "-q job scheduler (atm supported: SLURM)"
}

function print_levels {
   echo "print option not working yet"
   exit
}

function prepare_input {
   debug_echo "$atom-$edge: preparing input files for method: $method, with charge compensation: $chrgcomp"

   # check for .lcore file in parent directory
   ls ../.lcore &> /dev/null && touch .lcore
   
   if [ "$chrgcomp" == "bg" ]
   then
      if [ "$method" == slater ]
      then
         sed -i '1s/ 0.0/-0.5/g' $ccase.inm
      elif [ "$method" == "dscf" ]
      then
         sed -i '1s/ 0.0/-1.0/g' $ccase.inm
      fi
   elif [ "$chrgcomp" == "cond" ]
   then
      nele=$(awk '{if (NR == 2) print $2}' $ccase.in2$complex)
      if [ "$method" == slater ]
      then
         nelenew=$(echo $nele | sed 's/\.0/\.5/g')
      elif [ "$method" == "dscf" ]
      then
         tmp=$(echo $nele | grep -o "^[0-9]*")
         nelenew=$(sed "s/$tmp/$((tmp + 1))/g" <<< $nele)
      fi
      sed -i "2s/$nele/$nelenew/g" $ccase.in2$complex
   fi
   incline=$((line[$atom]+edge))
   if [ "$method" == "slater" ]
   then
      charge="0.5"
   elif [ "$method" == "dscf" ]
   then
      charge="1"
   fi
   # FIXME: this will break if not all atoms before have single multiplicity
   awk -F "," -v n=$incline -v c=$charge '{if (NR==n){printf "%1i,%1i,%.1f\n", $1, $2, $3-c} else print}' $ccase.inc > tmp.inc
   mv tmp.inc $ccase.inc
}

# Parse required data from the out files
function parse_output {
   debug_echo "$atom-$edge: parsing output"
   greppatern=$(printf "%-3s%0*d" "${edgenames[$edge]}" 3 $atom)

   if [ "$method" == "forb" ]
   then
      local FILE=$case
      local OUTFILE="XPS-shifts-$method.txt"
   else
      local FILE=$ccase
      local OUTFILE="../XPS-shifts-$method-$chrgcomp.txt"
   fi

   efermi=$(grep \:FER $FILE.scf2 | grep -Eo "\-?[0-9]+\.[0-9]+")
   if [ "$method" == "slater" ] || [ "$method" == "forb" ]
   then
      ecore=$(grep "$greppatern" $FILE.scfc | grep -o "\-[0-9]*\.[0-9]*")
      energy=$(echo "$efermi - $ecore" | bc)
      echo $atom ${at_names[$atom]} $edge ${edgenames[$edge]} $ecore $efermi $energy >> $OUTFILE
   elif [ "$method" == "dscf" ]
   then
      etot=$(grep \:ENE ../$case.scfm | grep -Eo "\-?[0-9]+\.[0-9]+")
      etotnew=$(grep \:ENE $FILE.scfm | grep -Eo "\-?[0-9]+\.[0-9]+")
      if [ "$chrgcomp" == "bg" ]
      then
         energy=$(echo "$etotnew - $etot + $efermi" | bc)
      elif [ "$chrgcomp" == "cond" ]
      then
         energy=$(echo "$etotnew - $etot" | bc)
      fi
      echo $atom ${at_names[$atom]} $edge ${edgenames[$edge]} $etot $etotnew $energy >> $OUTFILE
   fi
}

function debug_echo {
   if [ "$DEBUG" == 1 ]
   then
      echo $1
   fi
}

# We don't have converged case, lets find out why
function check_status {
#FIXME: flatten this
   if [ "$SCHEDULER" != "" ]
   then
      if [ -f ".jobid" ]
      then
         debug_echo "Found a .jobid file"
         if [ "$SCHEDULER" == "S" ]
         then
            local JOBID=$(cat .jobid)
            if sacct -j $JOBID | grep -q "$(cat .jobid) .* RUNNING\|$(cat .jobid) .* PENDING"
            then
               echo $atom-$edge: still running with jobid $JOBID
               FINISHED=0
            else
               if [ -f ".problem" ]
               then
                  debug_echo "$atom-$edge: Job was already restarted and still fails, giving up"
               else
                  echo "$atom-$edge: Job $JOBID is not running, cleaning up and trying again"
                  clean_lapw -s
                  touch .problem
                  run_job
               fi
            fi
         elif [ "$SCHEDULER" == "P" ]
         then
            local JOBID=$(cat .jobid)
            if qstat $JOBID > /dev/null
            then
               echo $atom-$edge: still running with jobid $JOBID
               FINISHED=0
            else
               if [ -f ".problem" ]
               then
                  debug_echo "$atom-$edge: Job was already restarted and still fails, giving up"
               else
                  echo "$atom-$edge: Job $JOBID is not running, cleaning up and trying again"
                  clean_lapw -s
                  touch .problem
                  run_job
               fi
            fi
         fi
      else
         debug_echo "Scheduler specified but no .jobid found, starting again"
         clean_lapw -s
         run_job
      fi
   fi
}

# Start the Wien2k calculation with the specified scheduler
function run_job {
   if [ "$SCHEDULER" == "S" ]
   then
      if [ -v jobfile ]
      then
         local JOBID=$(sbatch $jobfile |& grep "Submitted batch job" | grep -o "[0-9]*")
         if [ "$?" != "0" ]
         then
            echo "Error: SLURM submission failed"
         else 
            printf $JOBID > .jobid
         fi
      fi
   elif [ "$SCHEDULER" == "P" ]
   then
      if [ -v jobfile ]
      then
         local JOBID=$(qsub $jobfile)
         if [ "$?" != "0" ]
         then
            echo "Error: SLURM submission failed"
         else 
            printf $JOBID > .jobid
         fi
      fi
   # check if we have a job script
   elif [ -v jobfile ]
   then
      $jobfile
   else
      # replace with a custum command if needed
      SCRATCH=. run_lapw -ec 0.0001 -cc 0.001 -p
   fi
   FINISHED=0
}

DEBUG=0
DRY=0
PRINT_LEVELS=0
SCHEDULER=""
FINISHED=1

while getopts "h?vdpf:q:c:m:" opt; do
   case "$opt" in
   h|\?)
      help
      exit 0
      ;;
   p) PRINT_LEVELS=1
      ;;
   f) jobfile=$(readlink -f $OPTARG)
      # we need get the absolute path since we will call it from
      # a different directory
      ;;
   v) DEBUG=1
      ;;
   c) if [ $OPTARG = "cond" ]
      then
         chrgcomp="cond"
      elif [ $OPTARG = "bg" ]
      then
         chrgcomp="bg"
      else
         debug_echo "Unknown method $OPTARG selected, using the default one"
         chrgcomp="bg"
      fi
      ;;
   m) if [ $OPTARG = "slater" ]
      then
         method="slater"
      elif [ $OPTARG = "dscf" ]
      then
         method="dscf"
      elif [ $OPTARG = "forb" ]
      then
         method="forb"
      else
         debug_echo "Unknown method $OPTARG selected, using the default one"
         method="slater"
      fi
      ;;
   q) if [ $OPTARG = "SLURM" ]
      then
         SCHEDULER="S"
      elif [ $OPTARG = "PBS" ]
      then
         SCHEDULER="P"
      else
         echo "Unknown job scheduler: $OPTARG"
      fi
      ;;
   d) DRY=1
      ;;
   esac
done
shift $((OPTIND-1))

if (($# < 1))
then
   echo "Error: No arguments given"
   help
   exit 0
fi

edgenames=("" "1S" "2S" "2PP" "2P")

case=$(basename $PWD)

line=1
atoms=0

debug_echo "Parsing the struct and inc files"
while ((1))
do
   if sed -n "${line}p" $case.inc | grep -q ORBITALS
   then
       ((atoms++))
       firstline=$(grep -n "^ATOM" $case.struct | sed -n ${atoms}p | grep -o "^[0-9]*")
       lastline=$(grep -n "^LOCAL ROT" $case.struct | sed -n ${atoms}p | grep -o "^[0-9]*")
       atnameline=$((lastline - 1))
       multline=$((firstline + 1))
       ((line[$atoms]=line))
       at_names[$atoms]=$(sed -n "${atnameline}p" $case.struct | grep -o "^[A-Z][a-z]*")
       mult[$atoms]=$(sed -n "${multline}p" $case.struct | grep -o "MULT= *[0-9]*" | grep -o "[0-9]*")
       debug_echo "Found atom ${at_names[$atoms]} with multiplicity ${mult[$atoms]}"
   elif sed -n "${line}p" $case.inc | grep -q " 0"
   then
       break
   else
       ((atoms[$atoms]++))
   fi
   ((line++))
done

if [ "$PRINT_LEVELS" == "1" ]
then
   for ((i=1; i<=atoms; i++))
   do
      echo "Atom $i is ${at_names[$i]} with ${atoms[$i]} core levels and multiplicity ${mult[$i]}"
   done
fi

# check if we have a complex case
if [ -f $case.in1c ]
then
   debug_echo "Complex case detected"
   complex="c"
fi

debug_echo "Parsing arguments"
for arg in $@
do
   # divide into atomic part and edge part
   selected_atoms=$(echo $arg | cut -d ":" -f1)
   selected_atoms=$(echo $selected_atoms | sed 's/,/ /g')
   selected_edges=$(echo $arg | cut -d ":" -f2)
   selected_edges=$(echo $selected_edges | sed 's/,/ /g')

   # expand the edges into list of integers
   expanded_edges=""
   for i in $selected_edges
   do
      if echo $i | grep -q "-"
      then
         min=$(echo $i | cut -d "-" -f1)
         max=$(echo $i | cut -d "-" -f2)
         expanded_edges+=" $(seq $min $max)"
         continue
      fi
      expanded_edges+=" $i"
   done

   # expand the atoms into list of integers
   expanded_atoms=""
   for i in $selected_atoms
   do
      if echo $i | grep -q "-"
      then
         min=$(echo $i | cut -d "-" -f1)
         max=$(echo $i | cut -d "-" -f2)
         expanded_atoms+=" $(seq $min $max)"
      elif echo $i | grep -q "[A-Z][a-z]*"
      then
         # specifid atom name, find all atom numbers
         for ((j = 1; j <= atoms; j++))
         do
            if [ "$i" == "${at_names[j]}" ]
            then
               expanded_atoms+=" $j"
            fi
         done
      else
         expanded_atoms+=" $i"
      fi
   done

   # add to the edge list
   for i in $expanded_atoms
   do
      edge[$i]+=" $expanded_edges"
      edge[$i]=$(echo "${edge[$i]}" | tr ' ' '\n' | sort -n | uniq | tr '\n' ' ')
   done
done

# go over all combination and calculate the binding energies
for ((atom=1; atom<=atoms; atom++))
do

   if [ ! -z "${edge[$atom]}" ]
   then
      if [ "${mult[$atom]}" -gt "1" ]
      then
         echo "Error: Multiplicity for atom $atom is ${mult[$atom]}, fix your struct file!"
         continue
      fi
   fi

   for edge in ${edge[$atom]}
   do
      ccase="$atom-$edge-$method-$chrgcomp"
      debug_echo "running $ccase"
      
      if ((edge > atoms[atom]))
      then
         echo "Warning: edge $edge for atom $atom not present"
         continue
      fi

      if grep -q "^$atom [A-Z][a-z]* $edge" XPS-shifts-$method-$chrgcomp.txt
      then
         debug_echo "$ccase: already finished and parsed succesfully"
         continue
      elif [ "$method" == "forb" ]
      then
         parse_output
      elif [ -d "$ccase" ]
      then
         cd $ccase
         dayfile=$(tail -n5 $ccase.dayfile 2> /dev/null)
         if grep -q "ec cc fc and str_conv 1 1 1 1" <<< $dayfile && \
            [ -f $ccase.scfc ] && [ -f $ccase.scf2 ] && [ -f $ccase.scfm ]
         then
            parse_output
         else
            debug_echo "$ccase: Not yet converged"
            check_status
         fi
         cd ..
      else
         mv ${case}.scf ${case}.scfbackup
         save_lapw -a -d $ccase $ccase &> /dev/null
         mv ${case}.scfbackup ${case}.scf
         cd $ccase

         prepare_input
         if [ "$DRY" != "1" ]
         then
            run_job
         fi
         
         cd ..
      fi
   done
done

if [ "$FINISHED" == 0 ]
then
   exit 1
else
   exit 0
fi

# vim: set tabstop=3:softtabstop=3:shiftwidth=2
# vim: set expandtab:
