#!/bin/bash

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright (c) 2016-2021 Pavel Ondračka.

function help {
   echo "Script to calculate core electron binding energies using OpenMX"
   echo ""
   echo "Usage: XPS_openmx [options] [atoms:levels] [atoms:levels] ..."
   echo "atoms are selected by their number or name from input file"
   echo "multiple atoms can be selected with ',' as separator and"
   echo "'-' to mark ranges, eg. 1,2,5 or 5-10,12-17 or O,Ti ."
   echo "Core levels are selected with their names and a number,"
   echo "i.e., 1s1, or 3d2, where the second number is the orbital index, "
   echo "see http://www.openmx-square.org/openmx_man3.9/node192.html"
   echo ""
   echo "-h print this help and exit"
   echo "-v output more information"
   echo "-d dry run (don't start any calculations)"
   echo "-c [charge placement] where should the extra charge be placed:"
   echo "   conduction band (cond), background (bg) or molecule (m), (default bg)"
   echo "-f [filename] a job submission script (standard openmx otherwise) "
   echo "-q job scheduler (atm supported: SLURM)"
   echo "-r don't copy restart files (job file will do it)"
}

function prepare_input {
   debug_echo "$atom-$edge: preparing input files for OpenMX, with charge compensation: $chrgcomp"

   sed -i 's/\(scf\.coulomb\.cutoff\).*/\1 on/gI' $INFILE

   if [ "$chrgcomp" == "bg" ] || [ "$chrgcomp" == "m" ]
   then
      if grep -qi "scf.system.charge" $INFILE
      then
         sed -i 's/\(scf\.system\.charge\).*/\1 1.0/gI' $INFILE
      else
         echo "scf.system.charge 1.0" >> $INFILE
      fi
   fi

   # enable the restart and copy the restart files
   if [ "$chrgcomp" != "m" ]
   then
      echo "scf.restart on" >> $INFILE
      if [ "$RESTART" == "1" ]
      then
         printf "copying restart files ..."
         cp -r ../*_rst .
         echo "DONE"
      fi
      echo "scf.restart.filename $INITIAL_NAME" >> $INFILE
   fi
   sed -i "s/System.Name.*/System.Name $atom-$edge-$chrgcomp/g" $INFILE

   echo "scf.coulomb.cutoff on" >> $INFILE
   echo "scf.core.hole on" >> $INFILE
   echo "<core.hole.state" >> $INFILE
   sed "s/^[0-9]*\([a-Z]*\)/$atom \1 /g" <<< "$edge" >> $INFILE
   echo "core.hole.state>" >> $INFILE

   firstline=$(grep -in "<Atoms.SpeciesAndCoordinates" $INFILE | grep -o "^[0-9]*")
   debug_echo "prepare_input: changing atom $atom on line $((firstline + atom))"
   awk -v n=$atom -v s=$firstline '{if (NR - s == n) \
               {printf "%3i %5s1 %12.7f %12.7f %12.7f %4.1f %4.1f\n", $1, $2, $3, $4, $5, $6, $7} \
            else print}' $INFILE > tmp.dat
   mv tmp.dat $INFILE
}

declare -A level
level["1s1"]=1
level["1s2"]=1

# Parse required data from the out files
function parse_output {
   debug_echo "$atom-$edge: parsing output"
   etot=$(grep "Utot." ../*.out | grep -Eo "\-?[0-9]+\.[0-9]+" | tail -n1)
   debug_echo "$atom-$edge: energy of the initial state $etot Ha"
   etotnew=$(grep "Utot." *.out | grep -Eo "\-?[0-9]+\.[0-9]+")
   debug_echo "$atom-$edge: energy of the final state $etotnew Ha"

   if [ "$chrgcomp" == "bg" ]
   then
      efermi=$(grep "Chemical potential (Hartree)" ../*.out | grep -Eo "\-?[0-9]+\.[0-9]+" | tail -n1)
      debug_echo "$atom-$edge: chemical potential $efermi Ha"
      energy=$(echo "$etotnew - $etot + $efermi" | bc)
   elif [ "$chrgcomp" == "cond" ] || [ "$chrgcomp" == "m" ]
   then
      energy=$(echo "$etotnew - $etot" | bc)
   fi

   echo $atom ${at_names[$atom]} ${level["$edge"]} $edge $etot $etotnew $energy >> "../$OUTFILE"
}

function debug_echo {
   if [ "$DEBUG" == 1 ]
   then
      echo $1 $2
   fi
}

function check_input {
   #check that we have the CH VAO and VPS for the specified level
   edgetype=$(grep -o "[0-9][a-z]" <<< "$edge")
   TMPLINE=$(grep -m 1 -A $species '<Definition.of.Atomic.Species' $case | grep "^\s*${at_names[$atom]}1")
   if [ "$TMPLINE" == "" ]
   then
      echo "Error: you need to define species ${at_names[$atom]}1 for the atoms with corehole"
      exit 2
   else
      PAO=$(awk '{print $2}' <<< $TMPLINE)
      VPS=$(awk '{print $3}' <<< $TMPLINE)
      echo $PAO $VPS
      PAOedgetype=$(cut -f2 -d "_" <<< "$PAO" | grep -o "[0-9][a-z]")
      VPSedgetype=$(cut -f3 -d "_" <<< "$VPS" | grep -o "[0-9][a-z]")
      if ! grep -q "_CH-" <<< "$PAO"
      then
         echo "Error: no CH VAO detected for species ${at_names[$atom]}1"
         exit 3
      elif [ "$edgetype" != "$PAOedgetype" ]
      then
         echo "Error: wrong CH VAO detected for species ${at_names[$atom]}1, have $PAOedgetype, should have $edgetype"
         exit 4
      fi
      if ! grep -q "$edgetype" <<< "$VPSedgetype"
      then
         echo "Error: wrong VPS (bad level or no CH) detected for species ${at_names[$atom]}1, have $VPSedgetype, should have $edgetype"
         exit 5
      fi
   fi
   TMPLINE=$(grep -A $species '<Definition.of.Atomic.Species' $case | grep "^\s*${at_names[$atom]}\s")
   if [ "$TMPLINE" == "" ]
   then
      echo "Error: you need to define species ${at_names[$atom]}1 for the initial state calculation"
      exit 6
   else
      VPS=$(awk '{print $3}' <<< $TMPLINE)
      VPSedgetype=$(cut -f3 -d "_" <<< "$VPS" | grep -o "[0-9][a-z]")
      if ! grep -q "$edgetype" <<< "$VPSedgetype"
      then
         echo "Error: wrong VPS (bad level or no CH) detected for species ${at_names[$atom]}, have $VPSedgetype, should have $edgetype"
         exit 5
      fi
   fi
}

# We don't have results yet, lets find out why
function check_status {
   debug_echo "check_status: ${ccase}"
   
   #first check for a finished case
   stdoutsuf=""
   if [ -f ${ccase}.stdout${stdoutsuf} ] &&
         grep -q "The calculation was normally finished" "${ccase}.stdout${stdoutsuf}"
   then
      debug_echo "check status: calculation ${ccase}.dat${stdoutsuf} finished"
      # check that it is indeed converged
      # it could be that we just reached the maximum number of iterations
      niter=$(grep "* MD= " $ccase.stdout${stdoutsuf} |wc -l)
      nmaxiter=$(grep -i "scf.maxiter" $ccase.dat${stdoutsuf} | awk '{print $2}')
      if (( niter < nmaxiter ))
      then
         debug_echo "check_status: it seems we have a converged case (iterations: $niter, maximum: $nmaxiter)"
         return 0
      else
         debug_echo "check_status: maximum number of iterations reached"
      fi
   else
      debug_echo "check_status: the calculation did not finish properly, checking why..."
   fi

   #maybe the job is still running 
   if [ "$SCHEDULER" != "" ]
   then
      if [ -f ".jobid" ]
      then
         debug_echo "Found a .jobid file"
         if [ "$SCHEDULER" == "S" ]
         then
            local JOBID=$(cat .jobid)
            if sacct -j $JOBID | grep -q "$(cat .jobid) .* RUNNING\|$(cat .jobid) .* PENDING"
            then
               echo $atom-$edge: still running with jobid $JOBID
               FINISHED=0
               return 1
            else
               echo "Calculation did not converge properly, what you want to do?"
               #get rid of the stdout file so that it is not parsed when the job is recognized
               mv ${ccase}.stdout ${ccase}.stdoutold
               nmaxiter=$(grep "scf.maxIter" $ccase.dat | awk '{print $2}')
               criterion=$(grep "scf.criterion" $ccase.dat | awk '{print $2}')
               etemp=$(grep "scf.ElectronicTemperature" $ccase.dat | awk '{print $2}')
               echo "a) increase maximum number of iterations (currently: $nmaxiter)"
               echo "b) decrease required convergence criterion (currently: $criterion Ha)"
               echo "c) increase smearing temperature (currently: $etemp K)"
               echo "o) other (opens the input file in EDITOR)"
               read userinput
               while [ "$userinput" != "a" ] && [ "$userinput" != "b" ] && [ "$userinput" != "c" ] && [ "$userinput" != "o" ]
               do
                  sleep 1
                  echo "unrecognized option $userinput, try again"
                  read userinput
               done
               if [ "$userinput" != "o" ]
               then
                  echo "type the new value"
                  read uservalue
               else
                  $EDITOR $ccase.dat
               fi
               if [ "$userinput" == "a" ]
               then
                  sed -i "s/scf.maxIter.*/scf.maxIter $uservalue/g" $ccase.dat
               elif [ "$userinput" == "b" ]
               then
                  sed -i "s/scf.criterion.*/scf.criterion $uservalue/g" $ccase.dat
               elif [ "$userinput" == "c" ]
               then
                  sed -i "s/scf.ElectronicTemperature.*/scf.ElectronicTemperature $uservalue/g" $ccase.dat
               fi
               rm -rf *_rst
               if [ "$chrgcomp" != "m" ] && [ "$RESTART" == "1" ]
               then
                  printf "copying restart files again... "
                  cp -r ../*_rst .
                  echo "DONE"
               fi
               run_job
            fi
         else
            echo "check_status: unsupported scheduler"
            exit 10
         fi
      else
         debug_echo "Scheduler specified but no .jobid found, starting again"
         run_job
      fi
   fi
   return 1
}

# Start the OpenMX calculation with the specified scheduler
function run_job {
   debug_echo "run_job: starting $jobfile ${INFILE}${stdoutsuf}"
   if [ "$SCHEDULER" == "S" ]
   then
      if [ -v jobfile ]
      then
         local JOBID=$(sbatch $jobfile ${INFILE}${stdoutsuf} |& grep "Submitted batch job" | grep -o "[0-9]*")
         printf $JOBID > .jobid
      fi
   # check if we have a job script
   elif [ -v jobfile ]
   then
      $jobfile $INFILE
   else
      # replace with a custum command if needed
      openmx $INFILE
   fi
   FINISHED=0
}

DEBUG=0
DRY=0
PRINT_LEVELS=0
SCHEDULER=""
FINISHED=1
RESTART=1
chrgcomp="bg"

while getopts "h?vrdf:q:c:" opt; do
   case "$opt" in
   h|\?)
      help
      exit 0
      ;;
   f) jobfile=$(readlink -f $OPTARG)
      # we need get the absolute path since we will call it from
      # a different directory
      ;;
   r) RESTART=0
      ;;
   v) DEBUG=1
      ;;
   c) if [ $OPTARG = "cond" ]
      then
         chrgcomp="cond"
      elif [ $OPTARG = "bg" ]
      then
         chrgcomp="bg"
      elif [ $OPTARG = "m" ]
      then
         chrgcomp="m"
      else
         debug_echo "Unknown method $OPTARG selected, using the default one"
      fi
      ;;
   q) if [ $OPTARG = "SLURM" ]
      then
         SCHEDULER="S"
      else
         echo "Unknown job scheduler: $OPTARG"
      fi
      ;;
   d) DRY=1
      ;;
   esac
done
shift $((OPTIND-1))

if (($# < 1))
then
   echo "Error: No arguments given"
   help
   exit 0
fi

# Check that we have a jobscript of using scheduler
if [ ! -z $SCHEDULER ]
then
   if [ -z ${jobfile+x} ] || [ ! -f $jobfile ]
   then
      echo "Error: jobfile was not specified or does not exist"
      exit 10
   fi
fi

case=$(ls *.dat)
INITIAL_NAME=$(grep "System.Name" $case | awk '{print $2}')

debug_echo "Parsing input file"

if ! grep -iw "scf.SpinPolarization" $case | grep -q "\son"
then
   echo "OpenMX needs spinpolarized calculations for corehole"
   exit 1
fi

# check that we have restart file, this is needed for charged bulks, but also
# metals under some cinscumstances, see: http://www.openmx-square.org/forum/patio.cgi?mode=view&no=2654

if [ ! -d *_rst ] && [ "$chrgcomp" != "m" ]
then
   echo "Restart files not found"
   exit 11
fi

atoms=$(grep -m 1 -i "Atoms.Number" $case | awk '{print $2}')
species=$(grep -m 1 -i "Species.Number" $case | awk '{print $2}')
debug_echo "We have $atoms atoms ($species types)"

firstatline=$(grep -ni "<Atoms.SpeciesAndCoordinates" $case | grep -o "^[0-9]*")
for ((i = 1; i <= atoms; i++))
do
   l=$((firstatline + i))
   at_names[$i]=$(sed -n "${l}p" $case | awk '{print $2}') 
done
debug_echo "Found atoms ${at_names[*]}"

debug_echo "Parsing arguments"
for arg in $@
do
   # divide into atomic part and edge part
   selected_atoms=$(echo $arg | cut -d ":" -f1)
   selected_atoms=$(echo $selected_atoms | sed 's/,/ /g')
   expanded_edges=$(echo $arg | cut -d ":" -f2)
   expanded_edges=$(echo $expanded_edges | sed 's/,/ /g')

   # expand the atoms into list of integers
   expanded_atoms=""
   for i in $selected_atoms
   do
      if echo $i | grep -q "-"
      then
         min=$(echo $i | cut -d "-" -f1)
         max=$(echo $i | cut -d "-" -f2)
         expanded_atoms+=" $(seq $min $max)"
      elif echo $i | grep -q "[A-Z][a-z]*"
      then
         # specified atom name, find all atom numbers
         for ((j = 1; j <= atoms; j++))
         do
            if [ "$i" == "${at_names[j]}" ]
            then
               expanded_atoms+=" $j"
            fi
         done
      else
         expanded_atoms+=" $i"
      fi
   done

   # add to the edge list
   for i in $expanded_atoms
   do
      edge[$i]+=" $expanded_edges"
      edge[$i]=$(echo "${edge[$i]}" | tr ' ' '\n' | sort -n | uniq | tr '\n' ' ')
   done
done

OUTFILE=XPS-shifts-${chrgcomp}.txt

# go over all combination and calculate the binding energies
for ((atom=1; atom<=atoms; atom++))
do
   for edge in ${edge[$atom]}
   do

      ccase="$atom-$edge-$chrgcomp"
      INFILE=$ccase.dat
      debug_echo "running $ccase"
      check_input
      
      if grep -q "^$atom [A-Z][a-z]* ${level[${edge}]} $edge" "$OUTFILE" 2> /dev/null
      then
         debug_echo "$ccase: already finished and parsed succesfully"
         continue
      elif [ -d "$ccase" ]
      then
         cd $ccase
         if check_status
         then
            parse_output
         fi
         cd ..
      else
         mkdir $ccase
         cp $case $ccase/$ccase.dat
         cd $ccase

         prepare_input
         if [ "$DRY" != "1" ]
         then
            run_job
         fi
         
         cd ..
      fi
   done
done

if [ "$FINISHED" == 0 ]
then
   exit 1
else
   exit 0
fi

# vim: set tabstop=3 softtabstop=3 shiftwidth=2 expandtab:
