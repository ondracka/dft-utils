#!/bin/bash

D1s="-0.01 -0.005 0 0.005 0.01"
D2s="-0.02 -0.01 -0.005 -0.0025 0.0025 0.005 0.01 0.02"
D="$D2s"

#C11 C12
for i in $D
do
	mkdir D_$i
	cp INCAR POTCAR KPOINTS D_$i/
	awk "{if (NR == 3) print \$1 * (1 + $i), \$2, \$3; \
	else if (NR == 4) print \$1, \$2 * (1 - $i), \$3; \
	else if (NR == 5) print \$1, \$2, \$3 / (1 - $i*$i);
	else print}" CONTCAR > D_${i}/POSCAR
	sed -i "s/ISIF.*/ISIF = 2/g" D_${i}/INCAR
	cd D_$i
	sbatch $1
	cd ..
done

#C44 deformations
for i in $D
do
	mkdir D44_$i
	cp INCAR POTCAR KPOINTS D44_$i/
	awk "{if (NR == 3) {xx = \$1; yx = \$1 * $i;} \
	else if (NR == 4) {xy = $i * \$2; yy = \$2 } \
	else if (NR == 5) {print xx, xy, 0; print yx, yy, 0; print \$1, \$2, \$3 / (1 - $i*$i);} 
	else print}" CONTCAR > D44_${i}/POSCAR
	sed -i "s/ISIF.*/ISIF = 2/g" D44_${i}/INCAR
        cd D44_$i
        sbatch $1
        cd ..
done
